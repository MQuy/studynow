var express = require("express");
var cors = require('cors');
var app = express();
var path = require("path");
var MobileDetect = require('mobile-detect');

app.use(cors());
app.use(express.static('dist'));
app.use(require('prerender-node').set('prerenderToken', 'hyxb9SVFL4Qg1wPPgFuu'));

app.set('port', (process.env.PORT || 5000));

app.get('*', renderPartial);

app.listen(app.get('port'), function() {
  console.log("Node app is running on port:" + app.get('port'))
})

function renderPartial(req, res) {
  res.sendFile(path.join(__dirname + '/dist/index1.html'));
}
