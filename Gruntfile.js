module.exports = function (grunt) {
	grunt.initConfig({
		nggettext_extract: {
			pot: {
				files: {
					'src/template.pot': [
						'src/app/**/*.{jade,js}',
						'src/libraries/**/*.{jade,js}',
					]
				}
			},
		},

		nggettext_compile: {
			all: {
				files: [
					{
						expand: true,
						src: ['src/langs/*.po'],
						ext: '.js'
					}
				]
			},
		},

	});

	grunt.loadNpmTasks('grunt-angular-gettext');

	grunt.registerTask('prepare', ['nggettext_extract']);
	grunt.registerTask('build', ['nggettext_compile']);

	grunt.registerTask('default', ['prepare', 'build']);
};
