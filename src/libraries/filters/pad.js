'use strict';

function padFilter() {
  return function(value, b) {
    return value.pad(b);
  };
}

angular
  .module('filters.pad', [])
  .filter('pad', padFilter);
