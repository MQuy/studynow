'use strict';

require('./nl2br');
require('./range');
require('./pad');
require('./price');

angular.module('filters', [
  'filters.nl2br',
  'filters.range',
  'filters.pad',
  'filters.price'
]);
