'use strict';

function newlineFilter() {
  return function(value) {
    if (!value) return value;
    return value.replace(/\n\r?/g, '<br />');
  };
}

angular
  .module('filters.nl2br', [])
  .filter('nl2br', newlineFilter);
