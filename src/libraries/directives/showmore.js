'use strict';

function showmoreDirective() {
  var directive = {
    restrict: 'A',
    template: `
      <span ng-bind="vm.content"></span>
      <i class="fa fa-angle-double-{{vm.state}}" style="width: 24px; cursor: pointer" ng-click="vm.toggleState()">
    `,
    controller: showmoreCtrl,
    controllerAs: 'vm',
    bindToController: true,
    transclude: true,
    scope: true
  };

  return directive;
}

showmoreCtrl.$inject = ['$scope', '$attrs', '$transclude'];
function showmoreCtrl($scope, $attrs, $transclude) {
  var vm = this;

  $transclude((clone) => { vm.originalContent = vm.content = clone.text() });
  vm.toggleState = toggleState;
  vm.state = 'up';

  toggleState();

  return vm;

  function toggleState() {
    var limitedLength = $attrs.showmoreLimit || 100;
    var text = vm.content;
    var textLength = text.length + 4;
    var truncatedText;

    if (vm.state != 'right') {
      vm.state = 'right';
      if (textLength > limitedLength) {
        limitedLength = ` ${text}`.lastIndexOf(' ', limitedLength) || limitedLength;
        vm.content = `${text.substr(0, limitedLength)} ...`;
      }
    } else {
      vm.state = 'up';
      vm.content = vm.originalContent;
    }
  }
}

angular
  .module('directives.showmore', [])
  .directive('showmore', showmoreDirective);
