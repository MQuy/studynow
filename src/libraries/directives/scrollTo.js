'use strict';

function scrollToDirective() {
  var directive = {
    restrict: 'A',
    link: linkDirective
  };

  return directive;

  function linkDirective(scope, element, attrs) {
    element.find('li a').on('click', function() {
      var tagNav = $(this);
      var anchor = tagNav.attr('href').replace(/[#\/]/g, '');
      var tagScroll = $(`main .${anchor}`);
      var offsetTop = tagScroll.offset().top - 32;

      tagNav.closest('header').find('li').removeClass('active');
      tagNav.parent().addClass('active');
      $('html, body').animate({ scrollTop: offsetTop }, 500);
      return false;
    });
  }
}

angular
  .module('directives.scrollTo', [])
  .directive('scrollTo', scrollToDirective);
