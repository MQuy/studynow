function enterDirective() {
  var directive = {
    restrict: 'A',
    link: linkDirective
  };

  return directive;

  function linkDirective(scope, element, attrs) {
    element.bind('keydown keypress', function(event) {
      if(event.which === 13) {
        scope.$apply(function(){
          scope.$eval(attrs.ngEnter, {'$event': event});
        });
        event.preventDefault();
      }
    });
  }
}

angular
  .module('directives.ngEnter', [])
  .directive('ngEnter', enterDirective);
