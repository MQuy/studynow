'use strict';

function carouselDirective($timeout) {
  var directive = {
    restrict: 'A',
    link: linkDirective
  };

  return directive;

  function linkDirective(scope, element, attrs) {
    var tagSlider = $(element);

    $timeout(() => {
      tagSlider.slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 3000,
        nextArrow: '<i class="fa fa-angle-right"></i>',
        prevArrow: '<i class="fa fa-angle-left"></i>'
      });
    });
  }
}

carouselDirective.$inject = ['$timeout'];

angular
  .module('directives.carousel', [])
  .directive('carousel', carouselDirective);
