'use strict';

function alertDirectives() {
  var directive = {
    restrict: 'A',
    link: linkDirective,
    controllerAs: 'vm',
    controller: AlertCtrl
  };

  return directive;

  function linkDirective(scope, element, attrs) {
  };
}

var dbAnimateOut = _.debounce(animateOut, 3000);

function animateOut($element) {
  $element.removeClass('animate');
  setTimeout(() => {
    this.flashes = [];
  }, 500);
}

function dbFlashChange($scope, $element, value) {
  if (!Object.keys(value).length) return;

  var type = Object.keys(value)[0];

  $element.removeAttr('class').addClass('alert-' + type).addClass('animate');
  this.flashes = value.success || value.error;
  $scope.$safeApply();
  dbAnimateOut.call(this, $element);
}

class AlertCtrl {
  constructor($scope, $element, $flash) {
    this.fakeFlashes = $flash.all();

    $scope.$watch('vm.fakeFlashes', _.debounce(dbFlashChange.bind(this, $scope, $element)), true);
  }
}
AlertCtrl.$inject = ['$scope', '$element', '$flash'];

angular
  .module('directives.alert', [])
  .directive('alertAnimation', alertDirectives);
