'use strict';

require('./autofocus');
require('./alert/alert');
require('./ngEnter');
require('./plainSelect');
require('./scrollTo');
require('./dropdown');
require('./uploader');
require('./carousel');
require('./showmore');

angular.module('directives', [
  'directives.ngAutofocus',
  'directives.alert',
  'directives.ngEnter',
  'directives.plainSelect',
  'directives.scrollTo',
  'directives.dropdown',
  'directives.uploader',
  'directives.carousel',
  'directives.showmore'
]);
