'use strict';

function autoFocusDirective() {
  var directive = {
    restrict: 'A',
    link: linkDirective
  };

  return directive;

  function linkDirective(scope, element, attrs) {
    if (attrs.ngAutofocus === '' || scope.$eval(attrs.ngAutofocus)) {
      element.focus();
    }
  };
}

angular
  .module('directives.ngAutofocus', [])
  .directive('ngAutofocus', autoFocusDirective);
