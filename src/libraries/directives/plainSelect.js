var defaultOpts = {
  label: 'name'
};

selectDirective.$inject = ['$timeout'];
function selectDirective($timeout) {
  var directive = {
    restrict: 'AE',
    require: 'ngModel',
    scope: {
      plainSelect: '=',
      ngModel: '=',
      selectClass: '@',
      plainDefault: '@'
    },
    template: [
      '<ul class="hide" tabindex="9999">' +
        '<li ng-repeat="item in plainSelect" ng-class="{active: item.id == ngModel}" value="{{item.id}}">{{item.name}}</li>' +
      '</ul>' +
      '<div class="anchor" ng-bind="ngLabel || ngModel || plainDefault"  ng-class="selectClass"></div>'
    ].join(''),
    link: linkDirective
  };

  return directive;

  function linkDirective(scope, element, attrs) {
    var tagElement = angular.element(element);
    var tagWrapper = tagElement.parent();
    var tagAnchor = tagElement.children('.anchor');
    var tagUl = tagElement.children('ul');
    var parentScope = scope.$parent;
    var ngOptions = _.extend({}, defaultOpts, scope.$eval(attrs.plainOptions));
    var ngChange = attrs.ngChange;
    var minWidth, txtSearch, heightAnchor;
    var setLabelBind = setLabel.bind(this, scope, attrs, ngOptions, ngChange, tagUl);

    $timeout(function() {
      minWidth = tagWrapper.outerWidth();
      tagElement.addClass('plain-select');
      tagUl.css({width: minWidth});
      heightAnchor = tagWrapper.outerHeight();
      if (scope.ngModel) parentScope.$eval(ngChange);
      tagAnchor.bind('click', function(evt) {
        txtSearch = '';
        toggleUl(tagUl, tagElement, heightAnchor);
        setScroll(tagUl, tagUl.children('li.active'));
        return false;
      });
      tagUl
        .bind('mouseenter', function() {
          tagUl.focus();
        })
        .bind('mouseleave', function() {
          tagUl.blur();
        })
        .bind('keydown', function(evt) {
          var keyCode = evt.keyCode;
          if (keyCode === 13) setLabelBind(tagUl.find('li.active'));
          else if ((keyCode >= 65 && keyCode <= 90) ||
                    (keyCode >= 97 && keyCode <= 122)) {
            txtSearch += String.fromCharCode(keyCode).toLowerCase();
            checkSelectMatch(tagUl, txtSearch);
          }
        });
      tagUl
        .children('li')
        .bind('click', function(evt) {
          setLabelBind($(evt.target));
        })
        .bind('mouseenter', function(evt) {
          var target = $(evt.target);

          setActive(tagUl, target);
          setLabelBind(target, true);
        });
      $('select').bind('mousedown', function() {
        tagUl.addClass('hide');
      });
      $(document).bind('click', function() {
        tagUl.addClass('hide');
      });
    });

    directiveInit(scope, ngOptions);
  }
}

function directiveInit(scope, ngOptions) {
  scope.plainSelect.forEach(function(item) {
    if (item.id === scope.ngModel && ngOptions.label === 'name') {
      scope.ngLabel = item.name;
    }
  });
}

function setActive(tagUl, target) {
  tagUl.children('li').removeClass('active');
  $(target).addClass('active');
}

function setScroll(tagUl, target) {
  if (!target || !target.length) return;
  var tagFirst = tagUl.children('li:first');
  var targetTop = target.position().top;

  if (targetTop <= 0 || targetTop >= tagUl.outerHeight()) {
    tagUl[0].scrollTop = targetTop - tagFirst.position().top;
  }
  setActive(tagUl, target);
}

function checkSelectMatch(tagUl, str) {
  var tagMatch;
  var regex = new RegExp('^' + str);

  tagUl.children('li').each(function(index, ele) {
    var tagLi = $(ele);
    var liText = tagLi.text().toLowerCase();

    if (regex.test(liText)) {
      tagMatch = tagLi;
      return false;
    }
  });
  if (tagMatch) setScroll(tagUl, tagMatch);
}

function toggleUl(tagUl, tagElement, heightAnchor) {
  if (tagUl.hasClass('hide')) {
    tagUl.removeClass('hide').css({visibility: 'hidden'});
    if (tagElement.offset().top - $(window).scrollTop() + tagUl.outerHeight() + heightAnchor < $(window).height()) {
      tagUl.css({visibility: 'visible', top: heightAnchor, bottom: 'auto'});
    } else {
      tagUl.css({visibility: 'visible', top: 'auto', bottom: heightAnchor});
    }
    tagUl.focus();
  } else {
    tagUl.addClass('hide');
  }
  $('.plain-select ul').each(function(index, ele) {
    var tagEle = $(ele);

    if (!tagUl.is(tagEle)) tagEle.addClass('hide');
  });
}

function setLabel(scope, attrs, ngOptions, ngChange, tagUl, target, keepShow) {
  var value = target.attr('value');
  var label = target.text();
  var parentScope = scope.$parent;

  parentScope.$eval(vsprintf("%s = '%s'", [attrs.ngModel, value]));
  if (ngOptions.label === 'name') { scope.ngLabel = label; }
  parentScope.$eval(ngChange);
  scope.$apply();
  if (!keepShow) tagUl.addClass('hide');
}

angular
  .module('directives.plainSelect', [])
  .directive('plainSelect', selectDirective);
