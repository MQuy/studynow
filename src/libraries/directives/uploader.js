import Config from '../utils/config';

function uploaderDirective(currentUser) {
  var directive = {
    restrict: 'A',
    link: linkDirective.bind(this, currentUser)
  };

  return directive;

  function linkDirective(currentUser, scope, element, attrs) {
    element[0].addEventListener('change', handleFileSelect.bind(this, scope, currentUser, attrs), false);
  }

  function handleFileSelect(scope, currentUser, attrs, event) {
    var uploaderObject = scope.$eval(attrs.uploaderObject);
    var file = event.target.files[0];
    var formData = new FormData();
    var xhrRequest = new XMLHttpRequest();

    formData.append('key', uploaderObject.key);
    formData.append('acl', uploaderObject.acl);
    formData.append('success_action_status', uploaderObject.successActionStatus);
    formData.append('policy', uploaderObject.policy);
    formData.append('x-amz-algorithm', uploaderObject.algorithm)
    formData.append('x-amz-credential', uploaderObject.credential);
    formData.append('x-amz-date', uploaderObject.date);
    formData.append('x-amz-signature', uploaderObject.signature);
    formData.append('file', file, file.name);

    xhrRequest.upload.addEventListener('progress', uploadProgess.bind(this, scope), false);
    xhrRequest.addEventListener('load', uploadSuccess.bind(this, scope, uploaderObject, file), false);
    xhrRequest.addEventListener('error', uploadError, false);
    xhrRequest.addEventListener('abort', uploadAbort, false);

    xhrRequest.open('POST', uploaderObject.directFogUrl, true);

    xhrRequest.send(formData);
  }

  function uploadProgess(scope, evt) {
    var percentage;

    percentage = evt.lengthComputable ? Math.round(evt.loaded * 100 / evt.total) : 100;
    scope.$emit('uploadProgess', percentage);
  }

  function uploadSuccess(scope, uploaderObject, file, res) {
    var remotePath = uploaderObject.key.replace(/\$\{filename\}$/, file.name);
    var remoteUrl = uploaderObject.directFogUrl + remotePath;

    scope.$emit('uploadSuccess', { url: remoteUrl, fileName: file.name });
  }

  function uploadError() {
  }

  function uploadAbort() {
  }
}

uploaderDirective.$inject = ['currentUser'];

angular
  .module('directives.uploader', [])
  .directive('uploader', uploaderDirective);
