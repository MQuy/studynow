'use strict';

function dropdownDirective() {
  var directive = {
    restrict: 'A',
    link: linkDirective
  };

  return directive;

  function linkDirective(scope, element, attrs) {
    var tagTarget = $(attrs.dropdown);

    element.on('click', function() {
      $('.dropdown-menu').addClass('hide');
      tagTarget.removeClass('hide');

      return false;
    });

    tagTarget.on('click', function(evt) {
      evt.stopPropagation();
    })

    $('body').on('click', function() {
      $('.dropdown-menu').addClass('hide');
    });
  }
}

angular
  .module('directives.dropdown', [])
  .directive('dropdown', dropdownDirective);
