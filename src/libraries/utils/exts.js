(function(_) {
  _.empty = function(obj) {
    for (var key in obj) {
      obj.hasOwnProperty(key) && delete obj[key];
    }
    return obj;
  }

  _.isFloat = function(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  _.waitFor = function (testFx, onReady, timeOutMillis = 3000) {
    var start = new Date().getTime(),
    condition = false,
    interval = setInterval(function() {
      if ( (new Date().getTime() - start < timeOutMillis) && !condition ) {
        condition = (typeof(testFx) === 'string' ? eval(testFx) : testFx());
      } else {
        clearInterval(interval);
        if(!condition) {
          return;
        } else {
          typeof(onReady) === 'string' ? eval(onReady) : onReady();
        }
      }
    }, 100);
  };

  _.toUrlParams = function(json) {
    return Object.keys(json)
                 .map(function(key) { return encodeURIComponent(key) + "=" + encodeURIComponent(json[key]) })
                 .join("&")
                 .replace(/%20/g, "+");
  }

  Number.prototype.pad = function(b) {
    return (1e15 + this + '').slice(-b);
  }
})(typeof _ === 'undefined' ? (_ = {}) : _);
