import Config from './config';

class Facebook {
  constructor(currentUser, UserProvider, $location) {
    this.currentUser = currentUser;
    this.UserProvider = UserProvider;
    this.$location = $location;
    this.provider = {};

    _.waitFor(() => {
      return typeof FB != 'undefined'
    }, () => {
      FB.getLoginStatus(this.cbStatus.bind(this))
    });
  }
  cbStatus(fbRes) {
    if (fbRes.status == 'connected') {
      this.provider.token = fbRes.authResponse.accessToken
      FB.api('/me', this.cbMe.bind(this));
    } else {
      location.href = Config.fbAuthUrl;
    }
  }
  cbMe(fbRes) {
    _.merge(this.provider, fbRes, {uid: fbRes.id});
    this.UserProvider.create(this.provider).then((res) => {
      this.currentUser.setSession(res);
      this.$location.path('/profile');
    });
  }
}

export default Facebook;
