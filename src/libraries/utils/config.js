var Config = {};

Config.host = 'https://www.studynow.vn';
Config.apiUrl = 'https://www.studynow.vn/client/v1';
Config.fbScope = ['email', 'public_profile', 'user_friends'];
Config.fbAuthCbUrl = `${Config.host}/auth/facebook`;
Config.fbAuthUrl = `https://www.facebook.com/dialog/oauth?client_id=329926527085527&scope=${Config.fbScope.join(',')}&redirect_uri=${Config.fbAuthCbUrl}`;
Config.preAssetPaths = [];
Config.selectOpts = {
  weekly: [
    { label: 'Sunday', value: 0 },
    { label: 'Monday', value: 1 },
    { label: 'Tuesday', value: 2 },
    { label: 'Wednesday', value: 3 },
    { label: 'Thursday', value: 4 },
    { label: 'Friday', value: 5 },
    { label: 'Saturday', value: 6 }
  ],
  lessonLength: [
    { label: '30 min', value: 30 },
    { label: '60 min', value: 60 },
    { label: '90 min', value: 90 },
    { label: '120 min', value: 120 }
  ]
}
Config.timezone = moment().utcOffset() / 60 * 2;

Config.initValue = function(data) {
  _.each(data, (value, key) => {
    this[key] = value;
  });
}

export default Config;
