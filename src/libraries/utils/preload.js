'use strict';

class Preload {
  constructor(imageUrls, opts) {
    this.imageUrls = imageUrls;
    this.counter = 0; 
    this.opts = opts || {};
    this.execute();
  }
  execute() {
    _.each(this.imageUrls, (imageUrl) => {
      var img = new Image();

      img.onload = this.cbLoad.bind(this);
      img.src = imageUrl;
    });
    setTimeout(this.exceedTime.bind(this), 4000);
  }
  cbLoad() {
    this.counter++;
    if (this.counter == this.imageUrls.length) {
      this.calledLoaded = true;
      this.cbLoaded();
    }
  }
  exceedTime() {
    if (!this.calledLoaded) this.cbLoaded();
  }
  cbLoaded() {
    if (this.opts.cbLoaded) this.opts.cbLoaded();
  }
}

export default Preload;
