'use strict';

var invalidMap = {
  required: ' is empty',
  minlength: ' is too short!',
  maxlength: ' is too long!',
  pattern: ' is not in format'
};

function FormManager(vm, formName) {
  if (!(this instanceof FormManager)) {
    return new FormManager(vm, formName);
  }

  this.vm = vm;
  this.rawName = formName;

  vm.getClass = function(field) {
    var tagError = getErrorTag(vm[formName].$name, field);

    vm.checkFieldError(field) ? tagError.removeClass('hide') : tagError.addClass('hide');
  };

  vm.checkFieldError = function(field) {
    return vm[formName][field].$dirty && vm[formName][field].$invalid;
  };
}

FormManager.prototype.clearMessages = function() {
  this.showSuccesses([]);
  this.errorHandler();
};

FormManager.prototype.errorHandler = function() {
  var scopeForm = this.getFormScope();

  _.each(scopeForm, fieldHandler.bind(this, scopeForm));
};

FormManager.prototype.showErrors = function(errors) {
  var formName = this.getFormName();

  showResults(formName, '.successes', []);
  showResults(formName, '.errors', errors);
};

FormManager.prototype.showSuccesses = function(successes) {
  var formName = this.getFormName();

  showResults(formName, '.errors', []);
  showResults(formName, '.successes', successes);
};

FormManager.prototype.getErrorTag = function(field) {
  return getErrorTag(this.getFormName(), field);
};

FormManager.prototype.getErrorMessages = function() {
  var scopeForm = this.getFormScope();

  return _.select(_.map(scopeForm, getErrorMessage.bind(this, scopeForm)), (error) => { return error } );
}

FormManager.prototype.getFormScope = function() {
  return this.vm[this.rawName];
};

FormManager.prototype.getFormName = function() {
  return this.getFormScope().$name;
}

export default FormManager;

function fieldHandler(scopeForm, value, key) {
  if (!scopeForm[key] || !scopeForm[key].$setViewValue) return;

  var scopeField = scopeForm[key];
  var tagError = getErrorTag(scopeForm.$name, key);
  var msgError;

  scopeField.$setViewValue(scopeField.$viewValue);
  scopeField.$setDirty(true);

  if (scopeField.$invalid) {
    var keyError;

    _.each(scopeField.$error, function(value, key) {
      if (value) keyError = key;
    });

    msgError = inflection.humanize(key) + invalidMap[keyError];
    tagError.empty().append(`<span>${msgError}</span>`);
  } else {
    tagError.empty();
  }
}

function getErrorMessage(scopeForm, value, key) {
  if (!scopeForm[key] || !scopeForm[key].$setViewValue) return;

  var scopeField = scopeForm[key];
  var keyError;

  if (scopeField.$invalid) {
    _.each(scopeField.$error, function(value, key) {
      if (value) keyError = key;
    });
    return inflection.titleize(inflection.underscore(key)) + invalidMap[keyError];
  }
}

function getNoticeString(notice) {
  var noticeString = '';

  if (_.isString(notice)) {
    noticeString = `<p>${notice}</p>`;
  } else if (_.isObject(notice)) {
    Object.keys(notice).forEach(function(key) {
      noticeString += `<p>${inflection.titleize(key)} ${notice[key]}</p>`;
    });
  } else if (_.isArray(notice)) {
    notice.forEach(function(value) {
      noticeString += `<p>${value}</p>`;
    })
  }

  return noticeString;
}

function showResults(formName, className, notices) {
  var tagFields = angular.element(`[name="${formName}"]`).find(className);
  tagFields.empty();
  if (/Array/.test(Object.prototype.toString.call(notices))) {
    notices.forEach(function(notice) {
      tagFields.append(getNoticeString(notice));
    });
  } else {
    tagFields.append(getNoticeString(notices));
  }

  if (className === ".errors" && tagFields.length > 0)
    tagFields.focus();
}

function getElementTag(formName, elementName) {
  return angular.element(`[name="${formName}"]`).find(`[name="${elementName}"]`);
}

function getErrorTag(formName, elementName) {
  return getElementTag(formName, elementName).closest('.form-group').children('.error');
}
