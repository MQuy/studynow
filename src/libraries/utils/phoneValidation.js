'use strict';

var pattern = /^\+?([0-9]\d*)$/;

class PhoneValidation {
  constructor(number) {
    this.number = (number + "").replace(/[\s\.]+/g, '');
  }
  validate() {
    if (!pattern.test(this.number)) return false;
    if (/^09/.test(this.number)) {
      return this.mobilePhone(10);
    } else if (/^01/.test(this.number)) {
      return this.mobilePhone(11);
    } else {
      return this.cellPhone();
    }
  }
  mobilePhone(length) {
    return this.number.length == length;
  }
  cellPhone() {
    return this.number.length >= 8;
  }
}

export default PhoneValidation;
