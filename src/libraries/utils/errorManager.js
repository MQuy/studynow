'use strict';

var datum = {};
var errorManager = {};

errorManager.counter = counterErrors;
errorManager.get = getError;
errorManager.set = setError;
errorManager.clear = clearError;

function counterErrors() {
  return Object.keys(datum).length;
}

function setError(exception, cause) {
  if (typeof Rollbar != 'undefined') Rollbar.error(exception);
  datum[cause] = exception;
}

function clearError() {
  datum = {};
}

function getError(key) {
  return key ? datum[key] : datum;
}

export default errorManager;
