'use strict';

cacheService.$inject = ['CacheFactory'];
function cacheService(CacheFactory) {

  var localStoragePolyfill = {
    getItem: (key) => simpleStorage.get(key),
    setItem: (key, value) => simpleStorage.set(key, value),
    removeItem: (key) => simpleStorage.deleteKey(key)
  };

  var cache = CacheFactory('studynow', {
    maxAge: 60 * 60 * 1000,
    cacheFlushInterval: 60 * 60 * 1000,
    deleteOnExpire: 'aggressive',
    storageMode: 'localStorage',
    storageImpl: localStoragePolyfill
  });

  return cache;
};

angular
  .module('services.cache', [])
  .service('Cache', cacheService);
