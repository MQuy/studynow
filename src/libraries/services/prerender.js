'use strict';

var service = {};

function prerenderConfig() {
  this.$get = securityService;
}

prerenderReun.$inject = ['$injector'];
function prerenderReun($injector) {
  $injector.get('$prerender');
}

securityService.$inject = ['$rootScope', 'currentUser'];
function securityService($rootScope, currentUser) {

  $rootScope.$on('$routeChangeSuccess', function(event, next) {
    setTimeout(function() { window.prerenderReady = true }, 500)
  });

  return service;
}

angular
  .module('services.prerender', [])
  .provider('$prerender', prerenderConfig)
  .run(prerenderReun);
