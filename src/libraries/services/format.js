'use strict';

formatService.$inject = ['$injector', '$interpolate', '$rootScope', '$location'];
function formatService($injector, $interpolate, $rootScope, $location) {
  $rootScope.$on('$routeChangeSuccess', function(scope, current, pre) {
    var tagBody = $('body');

    window.scrollTo(0,0);
    setPageTitle(current.title);
    setPageDescription(current.description || current.title);
    setPageCanonical();

    tagBody.hasClass('initialize') ? tagBody.attr('class', 'initialize') : tagBody.removeAttr('class');

    _.each($location.path().split('/'), function(path) {
      if (path) tagBody.addClass(path.replace('_', '-'));
    });
  });

  $rootScope.$on('setPageHeader', function(e, args) {
    var title = args.title;
    var canonical = args.canonical;
    var description = args.description;

    setPageTitle(title);
    setPageDescription(description);
    setPageCanonical(canonical);
  });

  function setPageTitle(title) {
    title = ' ' + (title || '') + ' ';
    title = title.substring(0, title.lastIndexOf(' ', 120) || 120).trim();

    if (title) {
      $rootScope.pageTitle = title + ' | ' + 'Studynow';
    }
  }

  function setPageCanonical(path) {
    var url = $location.absUrl().split('?')[0];

    if (path) {
      url = url.split('/').slice(0, 3).join('/') + path;
    }

    $rootScope.pageCanonical = url;
  }

  function setPageDescription(description) {
    $rootScope.pageDescription = '';
  }
}

angular
  .module('services.format', [])
  .run(formatService);
