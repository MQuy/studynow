'use strict';

httpInterceptorConfig.$inject = ['$httpProvider'];
function httpInterceptorConfig($httpProvider) {
  $httpProvider.interceptors.push('httpInterceptor');
}

httpInterceptorService.$inject = ['$injector', '$q', '$flash', '$location'];
function httpInterceptorService($injector, $q, $flash, $location) {
  var services = {
    response: responseSuccess,
    responseError: responseError
  };

  return services;

  function responseSuccess(response) {
    var dataResponse = response.data;

    return response || $q.when(response);
  }

  function responseError(response) {
    var currentUser;

    switch(response.status) {
    case 401:
      currentUser = $injector.get('currentUser');
      currentUser.outSession();
    case 403:
      $location.path('/403');
      break;
    case 404:
      $location.path('/404');
      break;
    case 500:
      $location.path('/500');
      break;
    }
    return $q.reject(response);
  }
}

angular
  .module('services.httpInterceptor', ['services.flash'])
  .config(httpInterceptorConfig)
  .factory('httpInterceptor', httpInterceptorService);
