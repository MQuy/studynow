'use strict';

function popupConfig() {
  this.$get = popupService;
}

popupService.$inject = ['$http', '$compile', '$rootScope', '$templateCache', '$q', '$controller'];
function popupService($http, $compile, $rootScope, $templateCache, $q, $controller) {
  var exports = {};
  var defaults = {
    tagRoot: '#popup',
    tagClose: '.popup-content > .close',
    tagBody: '.popup-content',
    keyboard: true
  };
  var tagBody = angular.element('body');
  var tagPopup = angular.element(defaults.tagRoot);

  exports.create = createPopup;

  $rootScope.$on('$routeChangeStart', function() {
    _initTagPopup();
  });

  function Popup(opts) {
    var self = this, scope;
    _initTagPopup();
    self.options = _.extend({}, defaults, opts);
    scope = self.options.scope || angular.element('body').scope() || $rootScope;
    self.$scope = scope.$new();
    self.$scope.popup = this;
    _.each(self.options.scopeValue, function(value, key) {
      self.$scope[key] = value;
    });

    self.handledEscapeKey = function(e) {
      if (e.which === 27) {
        self.close();
        e.preventDefault();
        self.$scope.$apply();
      }
    };

    self.close = function() {
      $rootScope.$broadcast('popupClose');
      tagPopup.addClass('hide');
      self._unbindEvents();
      if (opts.cbClose) opts.cbClose();
    };
  }

  Popup.prototype.open = function(opts) {
    var self = this,
        options = self.options = angular.extend({}, self.options, opts);
    if (!options.templateUrl && !options.template) {
      throw new Error('Popup doesn\'t have template');
    }
    if (options.controller) {
      $controller(options.controller, {'$scope': self.$scope});
    }
    self._loadResources().then(function(data) {
      _initTagPopup();
      self.dom = $(data.$template).appendTo(tagPopup.empty().removeClass('hide')).closest(options.tagRoot).children(options.tagBody);
      $compile(tagPopup.children())(self.$scope);
      setTimeout(function() {
        self._bindEvents();
      }, 100);
    });
    self.$scope.$safeApply();
  };

  Popup.prototype._loadResources = function() {
    var self = this, templatePromise, options = self.options;

    if (options.template) {
      templatePromise = $q.when(options.template);
    } else if (options.templateUrl) {
      templatePromise = $http.get(options.templateUrl, {cache: $templateCache})
                              .then(function(response) { return response.data; });
    }
    return $q.when(templatePromise).then(function(value) {
      return {$template: value};
    });
  };

  Popup.prototype._bindEvents = function() {
    tagPopup.bind('click', this.close);
    tagPopup.find(this.options.tagClose).bind('click', this.close);
    if (this.options.keyboard) {
      tagBody.bind('keydown', this.handledEscapeKey);
    }
    this.dom.bind('click', this.preventChainEvent);
  };

  Popup.prototype._unbindEvents = function() {
    tagPopup.unbind('click', this.close);
    tagPopup.find(this.options.tagClose).unbind('click', this.close);
    if (this.options.keyboard) {
      tagBody.unbind('keydown', this.handledEscapeKey);
    }
    this.dom.unbind('click', this.preventChainEvent);
  };

  Popup.prototype.preventChainEvent = function(evt) {
    evt.stopPropagation();
  };

  return exports;

  function createPopup(opts) {
    return new Popup(opts);
  }

  function _initTagPopup() {
    tagPopup = angular.element(defaults.tagRoot);

    if (!tagPopup.length) {
      tagPopup = angular.element('<div>');
      tagPopup.attr('id', 'popup');
      tagBody.append(tagPopup);
    }
    tagPopup.addClass('hide').unbind('click').empty();
  }
}

angular
  .module('services.popup', [])
  .provider('$popup', popupConfig);
