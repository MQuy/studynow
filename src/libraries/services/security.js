'use strict';

var service = {};

function securityConfig() {
  this.requireAuthenticated = function(backUrl) {
    return service.handle(service.isAuthenticated, backUrl || '/');
  };

  this.requireUnauthenticated = function(backUrl) {
    return service.handle(service.isUnauthenticated, backUrl || '/');
  }
  this.$get = securityService;
}

securityRun.$inject = ['$injector'];
function securityRun($injector) {
  $injector.get('$security');
}

securityService.$inject = ['$rootScope', 'currentUser'];
function securityService($rootScope, currentUser) {

  $rootScope.$on('$routeChangeStart', function(event, next) {
    var security = next.$$route ? next.$$route.security : next.security;

    if (security && security() !== true) {
      next.redirectTo = security();
    }
  });

  service.isUnauthenticated = function() {
    return !service.isAuthenticated();
  };

  service.isAuthenticated = function() {
    return !!currentUser.id;
  };

  service.handle = function(execFn, backUrl) {
    return execFn() || backUrl || '/';
  };

  return service;
}

angular
  .module('services.security', [])
  .provider('$security', securityConfig)
  .run(securityRun);
