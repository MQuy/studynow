'use strict';

var service = {};

function languageConfig() {
  this.$get = securityService;
}

languageRun.$inject = ['$injector'];
function languageRun($injector) {
  $injector.get('$language');
}

securityService.$inject = ['$rootScope', 'gettextCatalog', 'currentUser'];
function securityService($rootScope, gettextCatalog, currentUser) {

  $rootScope.$on('$routeChangeSuccess', function(event, next) {
    if (next.params.lang) {
      gettextCatalog.setCurrentLanguage(next.params.lang);
    }
  });

  return service;
}

angular
  .module('services.language', [])
  .provider('$language', languageConfig)
  .run(languageRun);
