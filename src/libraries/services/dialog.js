'use strict';

function dialogConfig() {
  this.$get = dialogService;
}

dialogService.$inject = ['$http', '$compile', '$templateCache', '$rootScope', '$q', '$controller'];
function dialogService($http, $compile, $templateCache, $rootScope, $q, $controller) {
  var exports = {};
  var defaults = {
    rootSelector: 'main',
    tagClose: '.close'
  };

  exports.create = createDialog;

  function Dialog(opts) {
    var scope;

    this.options = _.extend({}, defaults, opts);
    scope = this.options.scope || angular.element('body').scope() || $rootScope;
    this.$scope = scope.$new();
    this.$scope.$dialog = this;
    _.each(this.options.scopeValue, (value, key) => {
      this.$scope[key] = value;
    });
  }

  Dialog.prototype.close = function() {
    this.dom.addClass('hide');
    this._unbindEvents();
  }

  Dialog.prototype.open = function(opts) {
    var options = this.options = _.extend({}, this.options, opts);
    var tagRoot = angular.element(options.rootSelector);
    var tagTarget = $(options.target);
    var pTarget = tagTarget.offset();

    if (options.only) {
      $('.dialog').remove();
    }
    if (!options.templateUrl && !options.template) {
      throw new Error('Dialog doesn\'t have template');
    }
    if (options.controller) {
      $controller(options.controller, {'$scope': this.$scope});
    }
    this._loadResources().then((data) => {
      this.dom = $(data.$template);
      if (pTarget) this.dom.offset({top: pTarget.top + tagTarget.outerHeight(), left: pTarget.left + tagTarget.outerWidth() / 2})
      this.dom.appendTo(tagRoot);
      $compile(this.dom)(this.$scope);
      setTimeout(() => {
        this._bindEvents();
      }, 100);
    });
    this.$scope.$safeApply();
  }

  Dialog.prototype._loadResources = function() {
    var templatePromise, options = this.options;

    if (options.template) {
      templatePromise = $q.when(options.template);
    } else if (options.templateUrl) {
      templatePromise = $http.get(options.templateUrl, {cache: $templateCache})
                              .then(function(response) { return response.data; });
    }
    return $q.when(templatePromise).then(function(value) {
      return {$template: value};
    });
  };

  Dialog.prototype._bindEvents = function() {
    this.dom.find(this.options.tagClose).on('click', this.close.bind(this));
  };

  Dialog.prototype._unbindEvents = function() {
    this.dom.find(this.options.tagClose).off('click', this.close.bind(this));
  };

  return exports;

  function createDialog(opts) {
    return new Dialog(opts);
  }
}

angular
  .module('services.dialog', [])
  .provider('$dialog', dialogConfig);
