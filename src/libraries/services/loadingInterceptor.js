'use strict';

loadingInterceptorConfig.$inject = ['$httpProvider'];
function loadingInterceptorConfig($httpProvider) {
  $httpProvider.interceptors.push('loadingInterceptor');
}

loadingInterceptorService.$inject = ['$injector', '$q'];
function loadingInterceptorService($injector, $q) {
  var $http;
  var tagLoading = angular.element('#loading');
  var services = {
    request: beforeRequest,
    requestError: doneErrorRequest,
    response: doneRequest,
    responseError: doneErrorRequest
  };

  return services;

  function beforeRequest(config) {
    tagLoading.attr('processing', '');
    return config || $q.when(config);
  }

  function doneRequest(response) {
    updateLoading();
    return response;
  }

  function doneErrorRequest(response) {
    updateLoading();
    return $q.reject(response);
  }

  function updateLoading() {
    $http = $http || $injector.get('$http');
    if ($http.pendingRequests.length < 1) {
      tagLoading.removeAttr('processing');
    }
  }
}

angular
  .module('services.loadingInterceptor', [])
  .config(loadingInterceptorConfig)
  .factory('loadingInterceptor', loadingInterceptorService);
