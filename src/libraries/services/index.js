'use strict';

require('./format');
require('./flash');
require('./localStorage');
require('./httpInterceptor');
require('./loadingInterceptor');
require('./popup');
require('./security');
require('./dialog');
require('./cache');
require('./prerender');
require('./language');

angular.module('services', [
  'services.format',
  'services.flash',
  'services.localStorage',
  'services.security',
  'services.httpInterceptor',
  'services.loadingInterceptor',
  'services.popup',
  'services.dialog',
  'services.cache',
  'services.prerender',
  'services.language'
]);
