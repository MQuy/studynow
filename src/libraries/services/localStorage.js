'use strict';

function localStorageConfig() {
  this.$get = localStorageService;
}

function localStorageService() {
  var exports = {};

  exports.set = function(key, value) {
    simpleStorage.set(key, value);
  }

  exports.get = function(key) {
    return simpleStorage.get(key);
  }

  exports.clear = function() {
    simpleStorage.flush();
  }

  return exports;
}

angular
  .module('services.localStorage', [])
  .provider('$localStorage', localStorageConfig);
