'use strict';

function UserFactory($http) {
  var baseUrl = '/users';

  return {
    signIn: function(user) {
      return $http.postData(baseUrl + '/sign_in', {user: user});
    },
    signUp: function(user) {
      return $http.postData(baseUrl + '/sign_up', {user: user});
    },
    me: function(params) {
      return $http.getData(baseUrl + '/me', {params: params});
    },
    update: function(id, user) {
      return $http.putData(baseUrl + '/' + user.id, {user: user});
    },
    lessonSchedules: function(id, params) {
      return $http.getData(`${baseUrl}/${id}/lesson_schedules`, { params: params });
    },
    upcomingSchedules: function(id, params) {
      return $http.getData(`${baseUrl}/${id}/upcoming_schedules`, { params: params });
    },
    forgotPassword: function(email) {
      return $http.postData(`${baseUrl}/forgot_password`, { email: email })
    },
    resetPassword: function(user) {
      return $http.postData(`${baseUrl}/reset_password`, { user: user })
    },
    getCourses: function(id, params) {
      return $http.getData(`${baseUrl}/${id}/courses`, { params: params });
    }
  }
}
UserFactory.$inject = ['$http'];

angular
  .module('resources.user', [])
  .factory('User', UserFactory);
