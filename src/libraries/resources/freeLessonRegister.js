'use strict';

function FreeLessonRegisterFactory($http) {
  var baseUrl = '/free_lesson_registers';

  return {
    create: function(params) {
      return $http.postData(baseUrl, { FreeLessonRegister: params });
    }
  }
}
FreeLessonRegisterFactory.$inject = ['$http'];

angular
  .module('resources.freeLessonRegister', [])
  .factory('FreeLessonRegister', FreeLessonRegisterFactory);
