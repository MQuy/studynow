'use strict';

function ConfigFactory($http) {
  var baseUrl = '/config';

  return {
    all: function() {
      return $http.getData(baseUrl);
    }
  }
}
ConfigFactory.$inject = ['$http'];

angular
  .module('resources.config', [])
  .factory('Config', ConfigFactory);
