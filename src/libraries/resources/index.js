'use strict';

require('./user');
require('./teacher');
require('./student');
require('./currentUser');
require('./userProvider');
require('./lessonSchedule');
require('./course');
require('./courseNote');
require('./comment');
require('./config');
require('./attachFile');
require('./uploader');
require('./freeLessonRegister');
require('./feedback');
require('./post');
require('./notification');
require('./homeworkStudent');

angular.module('resources', [
  'resources.user',
  'resources.teacher',
  'resources.student',
  'resources.currentUser',
  'resources.userProvider',
  'resources.lessonSchedule',
  'resources.courses',
  'resources.courseNote',
  'resources.comment',
  'resources.config',
  'resources.attachFile',
  'resources.uploader',
  'resources.freeLessonRegister',
  'resources.feedback',
  'resources.post',
  'resources.notification',
  'resources.homeworkStudent'
])
