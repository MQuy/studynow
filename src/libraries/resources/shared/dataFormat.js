'use strict';

import Config from '../../utils/config'

class DataFormat {
  teacher(teacher) {
    return formatTeacher(teacher);
  }
  student(student) {
    return formatTeacher(student);
  }
  weeklySchedules(weeklySchedules) {
    return formatWeeklySchedules(weeklySchedules);
  }
}

function formatTeacher(teacher) {
  teacher.weeklySchedules = formatWeeklySchedules(teacher.weeklySchedules);
  teacher.weeklyBlocks = formatWeeklyBlocks(teacher.weeklySchedules);

  return teacher;
}

export function formatWeeklyBlocks(weeklySchedules) {
  return _.flatten(_.map(weeklySchedules, function(ws) {
    let distance = moment().utcOffset() / 60 * 2;
    let blockBegin = ws.blockRangeBegin % 336;
    let blockEnd = ws.blockRangeEnd % 336;

    if (blockBegin < blockEnd) {
      return _.range(blockBegin, blockEnd);
    } else {
      return _.range(blockBegin, 336).concat(_.range(0, blockEnd));
    }

  }));
}

export function formatWeeklySchedules(weeklySchedules) {
  let wss = _.map(weeklySchedules, (ws) => {
    let distance = moment().utcOffset() / 60 * 2;

    ws.blockRangeBegin += distance;
    ws.blockRangeEnd += distance;

    if (ws.blockRangeBegin < 0) {
      ws.blockRangeBegin += 336;
      ws.blockRangeEnd += 336;
    }

    ws.blockRangeDayBegin = Math.floor(ws.blockRangeBegin / 48) % 7;
    ws.blockRangeDayEnd = Math.floor(ws.blockRangeEnd / 48) % 7;
    ws.blockRangeTimeBegin = formatTimeValue(ws.blockRangeBegin % 48);
    ws.blockRangeTimeEnd = formatTimeValue(ws.blockRangeEnd % 48);

    return ws;
  });

  return _.sortBy(wss, (ws) => ws.blockRangeBegin % 336);
}

function formatTimeValue(block) {
  return `${Math.floor(block / 2).pad(2)}:${(Math.floor(block % 2) * 30).pad(2)}`;
}

export default DataFormat;
