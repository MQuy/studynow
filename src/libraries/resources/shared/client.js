const Lokka = require('lokka').Lokka;
const HttpTransport = require('lokka-transport-http').Transport;

import { currentUser } from '../currentUser'

const headers = {
  token: currentUser.authenticationToken, email: currentUser.email
}

const graphql = new Lokka({
  transport: new HttpTransport('https://www.studynow.vn/graphql', headers)
})

const client = function(name, query, variables) {
  return graphql.query(query, variables).then(function(res) {
    return normalizeData(name ? res[name] : res);
  })
}

export default client;

function normalizeData(data) {
  var results = _.isArray(data) ? [] : {};

  _.each(data, function(value, key) {
    var nv = _.isObject(value) ? normalizeData(value) : value;
    results[_.isNumber(key) ? key : inflection.camelize(key, true)] = nv;
  });

  return results;
}
