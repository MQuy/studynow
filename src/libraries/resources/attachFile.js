'use strict';

function AttachFileFactory($http) {
  var baseUrl = '/attach_files';

  return {
    create: function(params) {
      return $http.postData(baseUrl, { attachFile: params });
    },
    update: function(id, params) {
      return $http.putData(`${baseUrl}/${id}`, { attachFile: params });
    }
  }
}
AttachFileFactory.$inject = ['$http'];

angular
  .module('resources.attachFile', [])
  .factory('AttachFile', AttachFileFactory);
