'use strict';

import client from './shared/client'

function PostFactory($http) {
  var baseUrl = '/posts';

  return {
    all: function(params) {
      return $http.getData(`${baseUrl}`, { params: params });
    },
    get: function(id) {
      return $http.getData(`${baseUrl}/${id}`);
    }
  }
}
PostFactory.$inject = ['$http'];

angular
  .module('resources.post', [])
  .factory('Post', PostFactory);
