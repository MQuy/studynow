'use strict';

import DataFormat from './shared/dataFormat'

function CourseFactory($http) {
  var baseUrl = '/courses';

  return {
    get: function(id) {
      return $http.getData(`${baseUrl}/${id}`).then(courseResponse);
    },
    teacherSchedules: function(id, params) {
      return $http.getData(`${baseUrl}/${id}/teacher_schedules`, { params: params });
    }
  }
}
CourseFactory.$inject = ['$http'];

angular
  .module('resources.courses', [])
  .factory('Course', CourseFactory);

function courseResponse(course) {
  new DataFormat().teacher(course.activeTeacher);

  return course;
}
