'use strict';

function FeedbackFactory($http) {
  var baseUrl = '/feedbacks';

  return {
    create: function(params) {
      return $http.postData(`${baseUrl}`, { feedback: params });
    }
  }
}
FeedbackFactory.$inject = ['$http'];

angular
  .module('resources.feedback', [])
  .factory('Feedback', FeedbackFactory);
