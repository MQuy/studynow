'use strict';

import Config from '../utils/config';
import DataFormat from './shared/dataFormat';

export let currentUser = {};

function currentUserFactory($http, $location, $window, $localStorage, gettextCatalog, User, Teacher, Student) {
  currentUser = $localStorage.get('currentUser') || {};

  currentUser.isLogin = function() {
    return !!currentUser.id;
  }

  currentUser.isTeacher = function() {
    return currentUser.role == 'teacher';
  }

  currentUser.isStudent = function() {
    return currentUser.role == 'student';
  }

  currentUser.getCourses = function(args) {
    return User.getCourses(currentUser.id, args);
  }

  currentUser.getProfile = function() {
    if (currentUser.isStudent()) {
      return Student.get(currentUser.id);
    } else {
      return Teacher.get(currentUser.id);
    }
  }

  currentUser.getPartners = function() {
    if (currentUser.isStudent()) {
      return Student.getTeachers(currentUser.id);
    } else {
      return Teacher.getStudents(currentUser.id);
    }
  }

  currentUser.getUpcomingSchedules = function(params) {
    return User.upcomingSchedules(currentUser.id, params);
  }

  currentUser.getLessonSchedules = function(params) {
    return User.lessonSchedules(currentUser.id, params);
  }

  currentUser.getSession = function() {
    if (!currentUser.id) return;

    return User.me().then(function(res) {
      currentUser.setSession(res);
    });
  }

  currentUser.setSession = function(res) {
    var user = formatUser(res);

    moment.tz.setDefault(user.timezone);
    _.merge(currentUser, user);
    if (currentUser.isStudent()) {
      gettextCatalog.setCurrentLanguage('vi');
    } else {
      gettextCatalog.setCurrentLanguage('en');
    }
    $localStorage.set('currentUser', user);
  }

  currentUser.outSession = function() {
    _.empty(currentUser);
    $localStorage.set('currentUser', currentUser);
    $window.location.href = '/';
  }

  return currentUser;
}

currentUserFactory.$inject = ['$http', '$location', '$window', '$localStorage', 'gettextCatalog', 'User', 'Teacher', 'Student'];

angular
  .module('resources.currentUser', ['resources.teacher', 'resources.student'])
  .factory('currentUser', currentUserFactory);

function formatUser(user) {
  if (user.role == 'teacher') {
    return new DataFormat().teacher(user);
  } else {
    return new DataFormat().student(user);
  }
}
