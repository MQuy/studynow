'use strict';

import DataFormat from './shared/dataFormat';

function TeacherFactory($http) {
  var baseUrl = '/teachers';

  return {
    get: function(id) {
      return $http.getData(baseUrl + '/' + id).then(function(teacher) {
        return new DataFormat().teacher(teacher);
      });
    },
    getStudents: function(id) {
      return $http.getData(`${baseUrl}/${id}/students`).then(studentsResponse);
    },
    updateWeeklySchedules: function(id, weeklySchedules) {
      convertWeeklyScheduleIntoZeroZone(weeklySchedules);
      return $http.putData(`${baseUrl}/${id}/weekly_schedules`, { teacher: { weeklySchedulesAttributes: weeklySchedules } }).then((weeklySchedules) => {
        return new DataFormat().weeklySchedules(weeklySchedules);
      });
    }
  }
}
TeacherFactory.$inject = ['$http'];

angular
  .module('resources.teacher', [])
  .factory('Teacher', TeacherFactory);

function studentsResponse(students) {
  return _.map(students, (student) => {
    return new DataFormat().student(student);
  })
}

function convertWeeklyScheduleIntoZeroZone(weeklySchedules) {
  return _.each(weeklySchedules, (ws) => {
    let distance = moment().utcOffset() / 60 * 2;

    ws.blockRangeBegin = ws.blockRangeDayBegin * 48 + _.sum(_.map(ws.blockRangeTimeBegin.split(':'), (x, y) => { return y == 0 ? +x * 2 : +x / 30 })) - distance;
    ws.blockRangeEnd = ws.blockRangeDayEnd * 48 + _.sum(_.map(ws.blockRangeTimeEnd.split(':'), (x, y) => { return y == 0 ? +x * 2 : +x / 30 })) - distance;

    if (ws.blockRangeBegin < 0) {
      ws.blockRangeBegin += 336;
    }
    if (ws.blockRangeEnd < 0) {
      ws.blockRangeEnd += 336;
    }
    if (ws.blockRangeBegin > ws.blockRangeEnd) {
      ws.blockRangeEnd += 336;
    }
  })
}
