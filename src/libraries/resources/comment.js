'use strict';

function CommentFactory($http) {
  var baseUrl = '/comments';

  return {
    all: function(params) {
      return $http.getData(baseUrl, { params: params }).then((comments) => {
        return _.map(comments, (comment) => {
          comment.timeFrom = moment(comment.createdAt).fromNow();

          return comment;
        })
      });
    },
    create: function(comment, attachFile) {
      return $http.postData(baseUrl, { comment: comment, attachFile: attachFile });
    }
  }
}
CommentFactory.$inject = ['$http'];

angular
  .module('resources.comment', [])
  .factory('Comment', CommentFactory);
