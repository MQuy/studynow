'use strict';

function CourseNoteFactory($http) {
  var baseUrl = '/course_notes';

  return {
    create: function(params) {
      return $http.postData(baseUrl, { courseNote: params });
    },
    destroy: function(id) {
      return $http.destroyData(`${baseUrl}/${id}`);
    }
  }
}
CourseNoteFactory.$inject = ['$http'];

angular
  .module('resources.courseNote', [])
  .factory('CourseNote', CourseNoteFactory);
