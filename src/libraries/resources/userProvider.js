'use strict';

function UserProviderFactory($http) {
  var baseUrl = '/user_providers';

  return {
    create: function(userProvider) {
      return $http.postData(baseUrl, {userProvider: userProvider});
    }
  }
}
UserProviderFactory.$inject = ['$http'];

angular
  .module('resources.userProvider', [])
  .factory('UserProvider', UserProviderFactory);
