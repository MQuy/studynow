'use strict';

import DataFormat from './shared/dataFormat';

function StudentFactory($http) {
  var baseUrl = '/students';

  return {
    get: function(id) {
      return $http.getData(`${baseUrl}/${id}`);
    },
    getTeachers: function(id) {
      return $http.getData(`${baseUrl}/${id}/teachers`).then(teachersResponse);
    }
  }
}
StudentFactory.$inject = ['$http'];

angular
  .module('resources.student', [])
  .factory('Student', StudentFactory);

function teachersResponse(teachers) {
  return _.map(teachers, (teacher) => {
    return new DataFormat().teacher(teacher);
  });
}
