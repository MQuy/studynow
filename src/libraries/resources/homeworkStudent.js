'use strict';

function homeworkStudentFactory($http) {
  var baseUrl = '/homework_students';

  return {
    all: function() {
      return $http.getData(baseUrl);
    },
    get: function(id) {
      return $http.getData(`${baseUrl}/${id}`);
    }
  }
}
homeworkStudentFactory.$inject = ['$http'];

angular
  .module('resources.homeworkStudent', [])
  .factory('HomeworkStudent', homeworkStudentFactory);
