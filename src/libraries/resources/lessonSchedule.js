'use strict';

function LessonScheduleFactory($http) {
  var baseUrl = '/lesson_schedules';

  return {
    create: function(params) {
      return $http.postData(baseUrl, { lessonSchedule: params });
    },
    get: function(id) {
      return $http.getData(`${baseUrl}/${id}`);
    },
    cancel: function(id) {
      return $http.postData(`${baseUrl}/${id}/cancel`);
    }
  }
}
LessonScheduleFactory.$inject = ['$http'];

angular
  .module('resources.lessonSchedule', [])
  .factory('LessonSchedule', LessonScheduleFactory);
