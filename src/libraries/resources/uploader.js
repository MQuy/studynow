'use strict';

function UploaderFactory($http) {
  var baseUrl = '/uploader';

  return {
    attachFile: function() {
      return $http.getData(`${baseUrl}/attach_file`);
    },
    avatar: function() {
      return $http.getData(`${baseUrl}/avatar`);
    }
  }
}
UploaderFactory.$inject = ['$http'];

angular
  .module('resources.uploader', [])
  .factory('Uploader', UploaderFactory);
