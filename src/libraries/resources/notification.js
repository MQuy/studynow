'use strict';

import client from './shared/client'

function NotificationFactory($http) {
  let baseUrl = '/notifications';

  return {
    all: function(params = {}) {
      let extraFields = params.page == 0 ? `unread_notifications_count(user_id: $user_id)` : '';
      return client(null, `
          query getNotifications($user_id: Int, $page: Int) {
            notifications(user_id: $user_id, page: $page) {
              id,
              action,
              notificable_type,
              created_at,
              data,
              creator {
                id,
                avatar,
                full_name
              }
            },
            ${extraFields}
          }
      `, params);
    },
    markAsRead: function(params) {
      return $http.postData(`${baseUrl}/mark_as_read`, params);
    }
  }
}
NotificationFactory.$inject = ['$http'];

angular
  .module('resources.notification', [])
  .factory('Notification', NotificationFactory);
