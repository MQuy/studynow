'use strict';

import errorManager from '../utils/errorManager';

angular
  .module('decorators.$exceptionHandler', [])
  .config(exceptionHandlerConfig);

exceptionHandlerConfig.$inject = ['$provide'];
function exceptionHandlerConfig($provide) {
  $provide.decorator('$exceptionHandler', exceptionHandlerDecorator);
}

exceptionHandlerDecorator.$inject = ['$delegate'];
function exceptionHandlerDecorator($exceptionHandler) {
  return function(exception, cause) {
    errorManager.set(exception, cause);
    $exceptionHandler(exception, cause);
  };
}
