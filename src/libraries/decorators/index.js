'use strict';

require('./exceptionHandler');
require('./http');
require('./rootScope');

angular.module('decorators', [
  'decorators.$exceptionHandler',
  'decorators.$http',
  'decorators.$rootScope'
]);
