'use strict';

require('./upcomingSchedules/upcomingSchedules');
require('./userPanel/userPanel');
require('./comments/comments');

angular.module('components.commons', [
  'components.commons.upcomingSchedules',
  'components.commons.userPanel',
  'components.commons.comments'
])
