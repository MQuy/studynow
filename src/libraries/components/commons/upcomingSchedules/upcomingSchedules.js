'use strict';

function upcomingSchedules() {
  var directive = {
    restrict: 'E',
    controller: directiveCtrl,
    templateUrl: 'libraries/components/commons/upcomingSchedules/upcomingSchedules.html',
    controllerAs: 'vm',
    scope: true,
    bindToController: {
      schedules: '='
    }
  };

  return directive;
}

class directiveCtrl {
  constructor($flash, LessonSchedule, gettextCatalog, currentUser) {
    this.$flash = $flash;
    this.LessonSchedule = LessonSchedule;
    this.gettextCatalog = gettextCatalog;
    this.currentUser = currentUser;
  }
  scheduleFormat(schedule) {
    let parterName = this.currentUser.isStudent() ? schedule.teacherName : schedule.studentName;

    return `${this.gettextCatalog.getString('You have a schedule at')} ${moment(schedule.startedTime).format('DD/MM/YYYY HH:mm')} ${this.gettextCatalog.getString('with')} ${parterName}`;
  }
  cancelSchedule(lessonSchedule) {
    this.LessonSchedule.cancel(lessonSchedule.id).then((res) =>{
      if (!res.errors) {
        location.reload();
      } else {
        this.$flash.now(res.errors[0], 'error');
      }
    });
  }
}

directiveCtrl.$inject = ['$flash', 'LessonSchedule', 'gettextCatalog', 'currentUser'];

angular
  .module('components.commons.upcomingSchedules', [])
  .directive('upcomingSchedules', upcomingSchedules);
