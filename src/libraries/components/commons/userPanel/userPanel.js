'use strict';

function userPanel() {
  var directive = {
    restrict: 'E',
    controller: directiveCtrl,
    templateUrl: 'libraries/components/commons/userPanel/userPanel.html',
    controllerAs: 'vm',
    scope: true,
    bindToController: {
      courses: '='
    }
  };

  return directive;
}

class directiveCtrl {
  constructor(currentUser) {
    this.currentUser = currentUser;
  }
  getPartner(course) {
    return this.currentUser.isStudent() ? course.teacher : course.student;
  }
}

directiveCtrl.$inject = ['currentUser'];

angular
  .module('components.commons.userPanel', [])
  .directive('userPanel', userPanel);
