'use strict';

let comments = {
  bindings: {
    commentableId: '=',
    commentableType: '=',
    comments: '=',
    uploaderObject: '='
  },
  templateUrl: 'libraries/components/commons/comments/comments.html',
  controllerAs: 'vm',
  controller: ['$scope', 'Comment', 'currentUser', function($scope, Comment, currentUser) {
    this.currentUser = currentUser;
    this.newAttachFile = undefined;
    this.comment = { userId: currentUser.id, commentableType: this.commentableType, commentableId: this.commentableId };
    this.createComment = createComment.bind(this);
    this.triggerFile = triggerFile.bind(this);
    this.removeAttachFile = removeAttachFile.bind(this);

    $scope.$on('uploadProgess', (evt, percentage) => {
      this.percentage = percentage;
      $scope.$apply();
      if (this.percentage != 100) return;
      setTimeout(() => {
        this.percentage = undefined;
        $scope.$apply();
      }, 500);
    })
    $scope.$on('uploadSuccess', (evt, res) => {
      this.newAttachFile = { url: res.url, fileName: res.fileName };
      $scope.$apply();
    })

    return this;

    function createComment() {
      if (this.newAttachFile || this.comment.content) {
        Comment.create(this.comment, this.newAttachFile).then((res) => {
          this.comments.unshift(res);
          this.comment.content = '';
          this.newAttachFile = undefined;
        })
      }
    }

    function triggerFile() {
      $('[name="attachFile[path]"]').trigger('click');
    }

    function removeAttachFile() {
      this.newAttachFile = undefined;
    }
  }]
}

angular
  .module('components.commons.comments', [])
  .component('comments', comments);
