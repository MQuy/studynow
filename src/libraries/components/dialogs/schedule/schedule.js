'use strict';

class DialogScheduleCtrl {
  constructor($scope, $flash, LessonSchedule, currentUser) {
    this.$flash = $flash;
    this.LessonSchedule = LessonSchedule;
    this.clickedTime = $scope.clickedTime;
    this.currentUser = currentUser;
    this.schedule = $scope.schedule;
    this.startedHour = moment(this.schedule.startedTime).format('HH:mm');
    this.endedHour = moment(this.schedule.endedTime).format('HH:mm');
    this.avatarUrl = currentUser.isTeacher() ? this.schedule.studentAvatar : this.schedule.teacherAvatar
  }
  timeRange() {
    return `${this.startedHour} - ${this.endedHour}`;
  }
  dateFormat() {
    return moment(this.schedule.startedTime).format('DD MMMM, YYYY');
  }
  cancelBooked() {
    this.LessonSchedule.cancel(this.schedule.id).then((res) => {
      if (!res.errors) {
        location.reload();
      } else {
        this.$flash.now(res.errors[0], 'error');
      }
    });
  }
  targetName() {
    return this.currentUser.isStudent() ? this.schedule.teacherName : this.schedule.studentName;
  }
  inFuture() {
    return this.clickedTime >= moment().subtract(6, 'hours');
  }
}

DialogScheduleCtrl.$inject = ['$scope', '$flash', 'LessonSchedule', 'currentUser'];

angular.module('components.dialogs.schedule', [])
  .controller('DialogScheduleCtrl', DialogScheduleCtrl);
