'use strict';

require('./schedule/schedule');
require('./book/book');
require('./books/books');

angular.module('components.dialogs', [
  'components.dialogs.schedule',
  'components.dialogs.book',
  'components.dialogs.books'
]);
