'use strict';

import Config from '../../../utils/config';
import FormManager from '../../../utils/formManager';

class DialogBooksCtrl {
  constructor($scope, $flash, LessonSchedule, currentUser) {
    this.$flash = $flash;
    this.LessonSchedule = LessonSchedule;
    this.currentUser = currentUser;
    this.formManager = new FormManager(this, 'LessonBookForm');
    this.courses = $scope.courses;
    this.startedTime = $scope.clickedTime;
    this.startedHour = moment(this.startedTime).format('HH:mm');
  }
  dateFormat() {
    return moment(this.startedTime).format('DD MMMM, YYYY');
  }
  formSubmit() {
    if (this.LessonBookForm.$valid) {

      this.LessonSchedule.create({
        startedTime: this.startedTime.toISOString(),
        courseId: this.courseId
      }).then((res) => {
        if (!res.errors) {
          location.reload();
        } else {
          this.$flash.now(res.errors[0], 'error');
        }
      });
    } else {
      this.formManager.showErrors(this.formManager.getErrorMessages());
    }
  }
  targetName() {
    return this.partner.nickname;
  }
}

DialogBooksCtrl.$inject = ['$scope', '$flash', 'LessonSchedule', 'currentUser'];

angular.module('components.dialogs.books', [])
  .controller('DialogBooksCtrl', DialogBooksCtrl);
