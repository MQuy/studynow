'use strict';

require('./sidebar/sidebar');

angular.module('components.navs', [
  'components.navs.sidebar'
]);
