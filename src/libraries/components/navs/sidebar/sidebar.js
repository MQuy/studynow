'use strict';

class NavSideBarCtrl {
  constructor($scope, $location, $popup, currentUser) {
  }
}

NavSideBarCtrl.$inject = ['$scope', '$location', '$popup', 'currentUser'];

angular.module('components.navs.sidebar', [])
  .controller('NavSideBarCtrl', NavSideBarCtrl);
