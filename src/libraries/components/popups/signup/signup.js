'use strict';

import FormManager from '../../../utils/formManager';

class PopupSignupCtrl {
  constructor($location, $popup, $localStorage, User) {
    this.$location = $location;
    this.$popup = $popup;
    this.$localStorage = $localStorage;
    this.User = User;
    this.user = { type: 'Student' };
    this.formManager = new FormManager(this, 'SignUpForm');
  }
  switchLogin() {
    this.$popup.create({
      templateUrl: 'libraries/components/popups/login/login.html',
      scope: this.$scope
    }).open();
  }
  formSubmit() {
    if (this.SignUpForm.$valid) {
      this.User.signUp(this.user).then((res) => {
        if (!res.errors) {
          _.merge(this.currentUser, res);
          this.$localStorage.set('currentUser', res);
          angular.element('#popup').addClass('hide');
          location.href = '/profile';
        } else {
          this.formManager.showErrors(res.errors);
        }
      });
    } else {
      this.formManager.errorHandler();
    }
  }
}

PopupSignupCtrl.$inject = ['$location', '$popup', '$localStorage', 'User'];

angular
  .module('components.popups.signup', [])
  .controller('PopupSignupCtrl', PopupSignupCtrl);
