'use strict';

import FormManager from '../../../utils/formManager';

class ForgotPasswordCtrl {
  constructor($location, $popup, User) {
    this.$location = $location;
    this.$popup = $popup;
    this.User = User;
    this.formManager = new FormManager(this, 'ForgotPasswordForm');
  }
  switchLogin() {
    this.$popup.create({
      templateUrl: 'libraries/components/popups/login/login.html',
      scope: this.$scope
    }).open();
  }
  formSubmit() {
    if (this.ForgotPasswordForm.$valid) {
      this.User.forgotPassword(this.email).then((res) => {
        if (!res.errors) {
          this.formManager.showSuccesses(res.messages);
        } else {
          this.formManager.showErrors(res.errors);
        }
      });
    } else {
      this.formManager.errorHandler();
    }
  }
}

ForgotPasswordCtrl.$inject = ['$location', '$popup', 'User'];

angular
  .module('components.popups.forgotPassword', [])
  .controller('PopupForgotPasswordCtrl', ForgotPasswordCtrl);
