'use strict';

require('./login/login');
require('./signup/signup');
require('./forgotPassword/forgotPassword');
require('./resetPassword/resetPassword');

angular.module('components.popups', [
  'components.popups.login',
  'components.popups.signup',
  'components.popups.forgotPassword',
  'components.popups.resetPassword'
]);
