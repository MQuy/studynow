'use strict';

import FormManager from '../../../utils/formManager';
import FbHandler from '../../../utils/facebook';

class PopupLoginCtrl {
  constructor($location, $popup, User, UserProvider, currentUser) {
    this.$location = $location;
    this.$popup = $popup;
    this.User = User;
    this.UserProvider = UserProvider;
    this.currentUser = currentUser;
    this.formManager = new FormManager(this, 'SignInForm');
  }
  switchSignUp() {
    this.$popup.create({
      templateUrl: 'libraries/components/popups/signup/signup.html',
      scope: this.$scope
    }).open();
  }
  switchForgotPassword() {
    this.$popup.create({
      templateUrl: 'libraries/components/popups/forgotPassword/forgotPassword.html',
      scope: this.$scope
    }).open();
  }
  formSubmit() {
    if (this.SignInForm.$valid) {
      this.User.signIn({email: this.email, password: this.password}).then((res) => {
        if (!res.errors) {
          this.currentUser.setSession(res);
          this.$location.path('/schedule');
          angular.element('#popup').addClass('hide');
        } else {
          this.formManager.showErrors(res.errors);
        }
      });
    } else {
      this.formManager.errorHandler();
    }
  }
  fbLogin() {
    new FbHandler(this.currentUser, this.UserProvider, this.$location);
  }
}

PopupLoginCtrl.$inject = ['$location', '$popup', 'User', 'UserProvider', 'currentUser'];

angular
  .module('components.popups.login', [])
  .controller('PopupLoginCtrl', PopupLoginCtrl);
