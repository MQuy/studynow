'use strict';

import FormManager from '../../../utils/formManager';

class PopupResetPasswordCtrl {
  constructor($scope, $location, $popup, User, currentUser) {
    this.$location = $location;
    this.$popup = $popup;
    this.User = User;
    this.currentUser = currentUser;
    this.formManager = new FormManager(this, 'ResetPasswordForm');
    this.token = $scope.token;
  }
  formSubmit() {
    if (this.password != this.passwordConfirmation) {
      this.formManager.showErrors(['Passsword is not the same as confirmation']);
      return;
    }
    if (this.ResetPasswordForm.$valid) {
      this.User.resetPassword({ token: this.token, password: this.password }).then((res) => {
        if (!res.errors) {
          this.currentUser.setSession(res);
          this.$location.path('/schedule').search({});
          angular.element('#popup').addClass('hide');
        } else {
          this.formManager.showErrors(res.errors);
        }
      });
    } else {
      this.formManager.errorHandler();
    }
  }
}

PopupResetPasswordCtrl.$inject = ['$scope', '$location', '$popup', 'User', 'currentUser'];

angular
  .module('components.popups.resetPassword', [])
  .controller('PopupResetPasswordCtrl', PopupResetPasswordCtrl);
