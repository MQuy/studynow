'use strict';

class HeaderSearchCtrl {
  constructor($scope, $location, $popup, Notification, currentUser) {
    this.$scope = $scope;
    this.$location = $location;
    this.Notification = Notification;
    this.currentUser = currentUser;
    this.notifications = [];
    this.notificationOffset = 0;
    this.noMoreLoad = false;
    this.loadingNotifications = false;
    this.loadMore();
  }
  logOut() {
    this.currentUser.outSession();
  }
  loadMore() {
    if (this.noMoreLoad || this.loadingNotifications) return;

    this.loadingNotifications = true;
    this.Notification.all({ user_id: this.currentUser.id, page: this.notificationOffset }).then((res) => {
      this.loadingNotifications = false;
      let notifications = _.map(res.notifications, (notification) => {
        notification.timeFrom = moment(notification.createdAt).fromNow();
        notification.targetName = `a ${inflection.underscore(notification.notificableType)}`;
        return notification;
      })
      if (notifications.length < 10) this.noMoreLoad = true;
      this.unreadCount = this.unreadCount || res.unreadNotificationsCount;
      this.notifications = this.notifications.concat(notifications);
      this.notificationOffset = Math.ceil(this.notifications.length / 10);
      this.$scope.$safeApply();
    });
  }
  markAsRead() {
    this.Notification.markAsRead({ userId: this.currentUser.id }).then(() => {
      this.unreadCount = 0;
    })
  }
  goToNotification(notification, $event) {
    $event.preventDefault();
    if (notification.read) {
      this.$location.path(`/schedule/${notification.data.courseCode}/curriculum`);
    } else {
      this.Notification.markAsRead({ id: notification.id }).then(() => {
        this.$location.path(`/schedule/${notification.data.courseCode}/curriculum`);
      })
    }
  }
}

HeaderSearchCtrl.$inject = ['$scope', '$location', '$popup', 'Notification', 'currentUser'];

angular.module('components.headers.search', [])
  .controller('HeaderSearchCtrl', HeaderSearchCtrl);
