'use strict';

require('./home/home');
require('./search/search');
require('./blog/blog');

angular.module('components.headers', [
  'components.headers.home',
  'components.headers.search',
  'components.headers.blog'
]);
