'use strict';

class HeaderBlogCtrl {
  constructor($scope, $location, $popup, currentUser) {
    this.$scope = $scope;
    this.$location = $location;
    this.$popup = $popup;
    this.anchor = $location.path();
    this.currentUser = currentUser;
  }
  logOut() {
    this.currentUser.outSession();
  }
  openLoginPopup() {
    this.$popup.create({
      templateUrl: 'libraries/components/popups/login/login.html',
      controller: 'PopupLoginCtrl as vm'
    }).open();
  }
}

HeaderBlogCtrl.$inject = ['$scope', '$location', '$popup', 'currentUser'];

angular.module('components.headers.blog', [])
  .controller('HeaderBlogCtrl', HeaderBlogCtrl);
