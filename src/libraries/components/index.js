'use strict';

require('./popups/index');
require('./headers/index');
require('./navs/index');
require('./dialogs/index');
require('./commons/index');

angular.module('components', [
  'components.popups',
  'components.headers',
  'components.navs',
  'components.dialogs',
  'components.commons'
]);
