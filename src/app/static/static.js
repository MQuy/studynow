'use strict';

function StaticController() {};

function StaticConfig($routeProvider) {
  $routeProvider
    .when('/500', {
      title: 'Error',
      templateUrl: 'app/static/500.html',
      controller: StaticController,
      controllerAs: 'vm'
    })
    .when('/404', {
      title: 'Not Found',
      templateUrl: 'app/static/404.html',
      controller: StaticController,
      controllerAs: 'vm'
    })
    .when('/403', {
      title: 'Permission Deined',
      templateUrl: 'app/static/403.html',
      controller: StaticController,
      controllerAs: 'vm'
    })
    .when('/policy', {
      title: 'Policy',
      templateUrl: 'app/static/policy.html',
      controller: StaticController,
      controllerAs: 'vm'
    })
    .when('/terms', {
      title: 'Terms',
      templateUrl: 'app/static/terms.html',
      controller: StaticController,
      controllerAs: 'vm'
    });
}

StaticConfig.$inject = ['$routeProvider'];

angular
  .module('static', ['ngRoute'])
  .config(StaticConfig);
