'use strict';

import ProfileCtrl from './profile.controller';

function ProfileConfig($routeProvider, $securityProvider) {
  $routeProvider
    .when('/profile', {
      title: 'Profile',
      templateUrl: 'app/profile/profile.html',
      security: $securityProvider.requireAuthenticated,
      controller: ProfileCtrl,
      controllerAs: 'vm',
      resolve: ProfileConfig.resolve
    });
}

ProfileConfig.$inject = ['$routeProvider', '$securityProvider'];

ProfileConfig.resolve = {
  courses: ['currentUser', function(currentUser) {
    if (currentUser.isStudent()) {
      return currentUser.getCourses({ 'statuses[]': ['in_process'] });
    }
  }],
  uploaderObject: ['Uploader', function(Uploader) {
    return Uploader.avatar();
  }]
};

angular
  .module('profile', ['ngRoute'])
  .config(ProfileConfig);
