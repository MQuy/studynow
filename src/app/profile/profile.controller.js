'use strict';

import Config from '../../libraries/utils/config';
import { formatWeeklyBlocks } from '../../libraries/resources/shared/dataFormat'
import FormManager from '../../libraries/utils/formManager'

class ProfileCtrl {
  constructor($scope, $popup, $flash, $localStorage, gettextCatalog, User, Teacher, currentUser, courses, uploaderObject) {
    this.$scope = $scope;
    this.$flash = $flash;
    this.$localStorage = $localStorage;
    this.gettextCatalog = gettextCatalog;
    this.User = User;
    this.Teacher = Teacher;
    this.currentUser = currentUser;
    this.user = _.pick(currentUser, 'fullName', 'nickname', 'phoneNumber', 'address', 'skype');
    this.weeklyOpts = Config.selectOpts.weekly;
    this.weeklySchedule = {};
    this.weeklySchedules = currentUser.weeklySchedules;
    this.remainOfLessons = _.sum(courses, (c) => { return c.remainOfLessons });
    this.profileForm = new FormManager(this, 'ProfileForm');
    this.weeklyScheduleForm = new FormManager(this, 'WeeklyScheduleForm');
    this.uploaderObject = uploaderObject;

    $scope.$on('uploadProgess', (evt, percentage) => {
      this.percentage = percentage;
      $scope.$safeApply();
      if (this.percentage != 100) return;
      setTimeout(() => {
        this.percentage = undefined;
        $scope.$safeApply();
      }, 500);
    })
    $scope.$on('uploadSuccess', (evt, res) => {
      this.currentUser.avatarUrl = res.url;
      this.User.update({id: this.currentUser.id, remoteAvatarTmpUrl: res.url}).then(() => {
        $flash.now(this.gettextCatalog.getString('Update avatar successfully'), 'success');
        $scope.$parent.$safeApply();
      })
    })
  }
  formSubmit() {
    if (this.ProfileForm.$valid) {
      this.User.update(this.currentUser.id, this.user).then((res) => {
        if (!res.errors) {
          this.$flash.now(this.gettextCatalog.getString('Update your profile successfully'), 'success');
          _.merge(this.currentUser, _.pick(res, 'fullName', 'nickname', 'phoneNumber', 'address', 'skype'));
          this.$localStorage.set('currentUser', this.currentUser);
        } else {
          this.profileForm.showErrors(res.errors);
        }
      });
    } else {
      this.profileForm.errorHandler();
    }
  }
  triggerFile() {
    $('[name="user[avatar]"]').trigger('click');
  }
  removeWeeklySchedule(weeklySchedule) {
    weeklySchedule['_destroy'] = 1;
  }
  addWeeklySchedule() {
    var weeklySchedule = _.merge(this.weeklySchedule, { teacherId: this.currentUser.id });

    if (this.weeklySchedule.blockRangeTimeBegin) {
      this.weeklySchedules.push(weeklySchedule);
      this.weeklySchedule = {};
    }
  }
  weeklySchedulesSubmit() {
    if (this.WeeklyScheduleForm.$valid) {
      var weeklySchedules = {};

      this.addWeeklySchedule();
      _.each(this.weeklySchedules, (ws, index) => { weeklySchedules[index] = ws });

      this.Teacher.updateWeeklySchedules(this.currentUser.id, weeklySchedules).then((res) => {
        if (!res.errors) {
          this.weeklySchedules = this.currentUser.weeklySchedules = res;
          this.currentUser.weeklyBlocks = formatWeeklyBlocks(res);
          this.$flash.now(this.gettextCatalog.getString('Update weekly schedule successfully'), 'success');
        } else {
          this.$flash.now(res.errors, 'error');
        }
      });
    } else {
      this.weeklyScheduleForm.errorHandler();
    }
  }
}

ProfileCtrl.$inject = ['$scope', '$popup', '$flash', '$localStorage', 'gettextCatalog', 'User', 'Teacher', 'currentUser', 'courses', 'uploaderObject'];

export default ProfileCtrl;
