'use strict';

class ScheduleEditCtrl {
  constructor($scope, $routeParams, CourseNote, currentUser,
      courses, upcomingSchedules, course) {
    this.$scope = $scope;
    this.CourseNote = CourseNote;
    this.courses = courses;
    this.currentUser = currentUser;
    this.course = course;
    this.courseNotes = course.courseNotes;
    this.courseNote = { courseId: course.id };
    this.partner = _.find(courses, (partner) => { return partner.slug == $routeParams.id });
    this.upcomingSchedules = upcomingSchedules;
  }
  formSubmit() {
    if (this.CourseNoteForm.$valid) {
      this.CourseNote.create(this.courseNote).then((res) => {
        this.courseNotes.unshift(res);
        this.courseNote = { courseId: this.course.id };
      })
    }
  }
}

ScheduleEditCtrl.$inject = [
  '$scope', '$routeParams', 'CourseNote', 'currentUser',
  'courses', 'upcomingSchedules', 'course'
];

export default ScheduleEditCtrl;
