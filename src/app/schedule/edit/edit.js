'use strict';

import ScheduleEditCtrl from './edit.controller';

function ScheduleEditConfig($routeProvider, $securityProvider) {
  $routeProvider
    .when('/schedule/:id/edit', {
      title: 'Course Curriculum Edit',
      templateUrl: 'app/schedule/edit/edit.html',
      security: $securityProvider.requireAuthenticated,
      controller: ScheduleEditCtrl,
      controllerAs: 'vm',
      resolve: ScheduleEditConfig.resolve
    })
}

ScheduleEditConfig.$inject = ['$routeProvider', '$securityProvider'];

ScheduleEditConfig.resolve = {
  courses: ['currentUser', function(currentUser) {
    return currentUser.getCourses({ 'statuses[]': ['in_process'], active: true });
  }],
  course: ['$route', 'Course', function($route, Course) {
    let courseId = $route.current.params.id;

    return Course.get(courseId);
  }],
  upcomingSchedules: ['$route', 'currentUser', function($route, currentUser) {
    var paramsUrl = $route.current.params;

    return currentUser.getUpcomingSchedules({ partnerId: paramsUrl.id });
  }]
}

angular
  .module('schedule.edit', ['ngRoute'])
  .config(ScheduleEditConfig);
