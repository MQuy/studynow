'use strict';

import ScheduleShowCtrl from './show.controller';

function ScheduleShowConfig($routeProvider, $securityProvider) {
  $routeProvider
    .when('/schedule/:id', {
      title: 'Schedule',
      templateUrl: 'app/schedule/show/show.html',
      security: $securityProvider.requireAuthenticated,
      controller: ScheduleShowCtrl,
      controllerAs: 'vm',
      resolve: ScheduleShowConfig.resolve
    })
}

ScheduleShowConfig.$inject = ['$routeProvider', '$securityProvider'];

ScheduleShowConfig.resolve = {
  courses: ['currentUser', function(currentUser) {
    return currentUser.getCourses({ 'statuses[]': ['in_process'], active: true });
  }],
  course: ['$route', 'Course', function($route, Course) {
    let courseId = $route.current.params.id;

    return Course.get(courseId);
  }],
  teacherLessonSchedules: ['$route', 'Course', 'currentUser', function($route, Course, currentUser) {
    let startedTime = moment().startOf('week').toISOString();
    let endedTime = moment().endOf('week').toISOString();
    let courseId = $route.current.params.id;

    return Course.teacherSchedules(courseId, { startedTime: startedTime, endedTime: endedTime });
  }],
  upcomingSchedules: ['$route', 'currentUser', function($route, currentUser) {
    let paramsUrl = $route.current.params;

    return currentUser.getUpcomingSchedules({ partnerId: paramsUrl.id });
  }]
};

angular
  .module('schedule.show', ['ngRoute'])
  .config(ScheduleShowConfig);
