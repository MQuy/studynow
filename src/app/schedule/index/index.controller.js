'use strict';

class ScheduleIndexCtrl {
  constructor($scope, $popup, $dialog, $flash, gettextCatalog, User, currentUser, courses, lessonSchedules, upcomingSchedules) {
    this.$scope = $scope;
    this.$popup = $popup;
    this.$dialog = $dialog;
    this.$flash = $flash;
    this.gettextCatalog = gettextCatalog;
    this.User = User;
    this.currentUser = currentUser;
    this.courses = courses;
    this.startedTime = moment().startOf('week');
    this.endedTime = moment().endOf('week');
    this.lessonSchedules = generateBlocks(lessonSchedules, this.startedTime);
    this.upcomingSchedules = upcomingSchedules;
  }
  showBook(day, time, $event) {
    var tagTarget = $($event.target);
    var block = day * 48 + time;
    var clickedTime = this.startedTime.clone().add(block * 30, 'minutes');

    if (tagTarget.css('cursor') != 'pointer') return;

    if (tagTarget.hasClass('booked') || tagTarget.hasClass('learnt')) {
      var ts = _.find(this.lessonSchedules, (ts) => { return ts.lessonBlocks.indexOf(block) != -1 });

      this.$dialog.create({
        templateUrl: 'libraries/components/dialogs/schedule/schedule.html',
        target: $event.target,
        only: true,
        scopeValue: {
          schedule: ts,
          clickedTime: clickedTime
        }
      }).open();
    } else if (tagTarget.hasClass('availability')) {
      if (clickedTime < moment()) {
        this.$flash.now(this.gettextCatalog.getString('You can modify your past schedule'), 'error');
        return;
      }

      this.$dialog.create({
        templateUrl: 'libraries/components/dialogs/books/books.html',
        target: $event.target,
        only: true,
        scopeValue: {
          clickedTime: clickedTime,
          courses: this.courses
        }
      }).open();
    }
  }
  blockClass(day, time) {
    var block = day * 48 + time;
    var currentTime = this.startedTime.clone().minute(block * 30);

    if (!this.currentUser.id) return;

    if (_.find(this.lessonSchedules, (ts) => { return ts.lessonBlocks.indexOf(block) != -1 })) {
      return currentTime > moment() ? 'booked' : 'learnt';
    } else if (this.currentUser.isTeacher() && this.currentUser.weeklyBlocks.indexOf(block) != -1) {
      return 'availability';
    }
  }
  calendarTitle() {
    var startFormat = 'DD MMMM, YYYY';

    if (this.startedTime.year() == this.endedTime.year()) {
      if (this.startedTime.month() == this.endedTime.month()) {
        startFormat = 'DD';
      } else {
        startFormat = 'DD MMMM';
      }
    }
    return `${this.startedTime.format(startFormat)} - ${this.endedTime.format('DD MMMM, YYYY')}`;
  }
  nextWeek(nav) {
    this.startedTime.add(nav, 'weeks');
    this.endedTime.add(nav, 'weeks');
    this.User.lessonSchedules(this.currentUser.id, {
      startedTime: this.startedTime.toISOString(),
      endedTime: this.endedTime.toISOString()
    }).then((lessonSchedules) => {
      this.lessonSchedules = generateBlocks(lessonSchedules, this.startedTime);
    });
  }
  getTimeZone() {
    var zone = moment().utcOffset() / 60;
    var neg = zone > 0 ? '+' : '-';

    return `GMT ${neg}${Math.abs(zone).pad(2)}`;
  }
}

ScheduleIndexCtrl.$inject = ['$scope', '$popup', '$dialog', '$flash', 'gettextCatalog', 'User', 'currentUser', 'courses', 'lessonSchedules', 'upcomingSchedules'];

export default ScheduleIndexCtrl;

function generateBlocks(lessonSchedules, anchorDate) {
  return _.map(lessonSchedules, (ts) => {
    var blockBegin = Math.round((new Date(ts.startedTime) - anchorDate) / (1000 * 60 * 30));
    var blockEnd = Math.round((new Date(ts.endedTime) - anchorDate) / (1000 * 60 * 30));

    ts.lessonBlocks = _.range(blockBegin, blockEnd);

    return ts;
  });
}
