'use strict';

import ScheduleIndexCtrl from './index.controller';

function ScheduleIndexConfig($routeProvider, $securityProvider) {
  $routeProvider
    .when('/schedule', {
      title: 'Schedule',
      templateUrl: 'app/schedule/index/index.html',
      security: $securityProvider.requireAuthenticated,
      controller: ScheduleIndexCtrl,
      controllerAs: 'vm',
      resolve: ScheduleIndexConfig.resolve
    });
}

ScheduleIndexConfig.$inject = ['$routeProvider', '$securityProvider'];

ScheduleIndexConfig.resolve = {
  courses: ['currentUser', function(currentUser) {
    return currentUser.getCourses({ 'statuses[]': ['in_process'], active: true });
  }],
  lessonSchedules: ['currentUser', function(currentUser) {
    var startedTime= moment().startOf('week').toISOString();
    var endedTime= moment().endOf('week').toISOString();

    return currentUser.getLessonSchedules({ startedTime: startedTime, endedTime: endedTime });
  }],
  upcomingSchedules: ['currentUser', function(currentUser) {
    return currentUser.getUpcomingSchedules();
  }]
};

angular
  .module('schedule.index', ['ngRoute'])
  .config(ScheduleIndexConfig);
