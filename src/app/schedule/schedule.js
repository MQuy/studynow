'use strict';

require('./index/index');
require('./show/show');
require('./curriculum/curriculum');
require('./edit/edit');

angular.module('schedule', [
  'schedule.index',
  'schedule.show',
  'schedule.curriculum',
  'schedule.edit'
]);
