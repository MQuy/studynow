'use strict';

import ScheduleCurriculumCtrl from './curriculum.controller';

function ScheduleCurriculumConfig($routeProvider, $securityProvider) {
  $routeProvider
    .when('/schedule/:id/curriculum', {
      title: 'Course Curriculum',
      templateUrl: 'app/schedule/curriculum/curriculum.html',
      security: $securityProvider.requireAuthenticated,
      controller: ScheduleCurriculumCtrl,
      controllerAs: 'vm',
      resolve: ScheduleCurriculumConfig.resolve
    })
}

ScheduleCurriculumConfig.$inject = ['$routeProvider', '$securityProvider'];

ScheduleCurriculumConfig.resolve = {
  courses: ['currentUser', function(currentUser) {
    return currentUser.getCourses({ 'statuses[]': ['in_process'], active: true });
  }],
  course: ['$route', 'Course', function($route, Course) {
    let courseId = $route.current.params.id;

    return Course.get(courseId);
  }],
  upcomingSchedules: ['$route', 'currentUser', function($route, currentUser) {
    var paramsUrl = $route.current.params;

    return currentUser.getUpcomingSchedules({ partnerId: paramsUrl.id });
  }],
  comments: ['$route', 'Comment', function($route, Comment) {
    var paramsUrl = $route.current.params;

    return Comment.all({ CommentableType: 'Course', CommentableId: paramsUrl.id });
  }],
  uploaderObject: ['Uploader', function(Uploader) {
    return Uploader.attachFile();
  }]
}

angular
  .module('schedule.curriculum', ['ngRoute'])
  .config(ScheduleCurriculumConfig);
