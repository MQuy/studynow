'use strict';

class ScheduleCurriculumCtrl {
  constructor($scope, $routeParams, CourseNote, currentUser,
      courses, upcomingSchedules, comments, course, uploaderObject) {
    this.CourseNote = CourseNote;
    this.courses = courses;
    this.currentUser = currentUser;
    this.comments = comments;
    this.course = course;
    this.courseNotes = course.courseNotes;
    this.uploaderObject = uploaderObject;
    this.upcomingSchedules = upcomingSchedules;
  }
  removeCourseNote(courseNote) {
    this.CourseNote.destroy(courseNote.id).then(() => {
      this.courseNotes.splice(_.findIndex(this.courseNotes, (cn) => {
        return cn.id == courseNote.id;
      }), 1);
    });
  }
}

ScheduleCurriculumCtrl.$inject = [
  '$scope', '$routeParams', 'CourseNote', 'currentUser',
  'courses', 'upcomingSchedules', 'comments', 'course', 'uploaderObject'
];

export default ScheduleCurriculumCtrl;
