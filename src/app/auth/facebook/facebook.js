'use strict';

import AuthFacebookCtrl from './facebook.controller';

function AuthFacebookConfig($routeProvider) {
  $routeProvider
    .when('/auth/facebook', {
      title: 'StudyNow',
      templateUrl: 'app/auth/facebook/facebook.html',
      controller: AuthFacebookCtrl,
      controllerAs: 'vm',
      resolve: AuthFacebookConfig.resolve
    });
}

AuthFacebookConfig.$inject = ['$routeProvider'];

AuthFacebookConfig.resolve = {
};

angular
  .module('auth.facebook', ['ngRoute'])
  .config(AuthFacebookConfig);
