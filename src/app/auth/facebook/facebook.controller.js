'use strict';

import FbHandler from '../../../libraries/utils/facebook';

class AuthFacebookCtrl {
  constructor($location, currentUser, UserProvider) {
    new FbHandler(currentUser, UserProvider, $location);
  }
}

AuthFacebookCtrl.$inject = ['$location', 'currentUser', 'UserProvider'];

export default AuthFacebookCtrl;
