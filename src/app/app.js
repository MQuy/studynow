'use strict';

require('../langs/vi');

require('../libraries/utils/exts');
require('../libraries/decorators/index');
require('../libraries/services/index');
require('../libraries/resources/index');
require('../libraries/components/index');
require('../libraries/filters/index');
require('../libraries/directives/index');
require('./home/home');
require('./profile/profile');
require('./checkout/checkout');
require('./schedule/schedule');
require('./homeworks/homeworks');
require('./static/static');
require('./auth/auth');
require('./courses/courses');
require('./freeLessons/freeLessons');
require('./blog/blog');

angular.module('studynow', [
  'infinite-scroll',
  'angular-cache',
  'gettext',
  'decorators',
  'resources',
  'services',
  'components',
  'filters',
  'directives',
  'home', 'schedule', 'homeworks', 'courses', 'profile', 'freeLessons', 'checkout', 'static', 'auth', 'blog'
]);

angular
  .module('studynow')
  .config(appConfig)
  .run(appRun);

appConfig.$inject = ['$routeProvider', '$locationProvider', '$httpProvider', '$sceProvider', '$compileProvider'];
function appConfig($routeProvider, $locationProvider, $httpProvider, $sceProvider, $compileProvider) {
  $sceProvider.enabled(false);
  $compileProvider.debugInfoEnabled(false);
  $locationProvider.html5Mode(true);
  $locationProvider.hashPrefix('#');
  $httpProvider.defaults.headers.common.Accept = 'application/json, text/plain';
  $routeProvider.otherwise({
    redirectTo: '/'
  });
}

appRun.$inject = ['$http', 'Cache', 'currentUser', 'gettextCatalog'];
function appRun($http, Cache, currentUser, gettextCatalog) {
  $http.defaults.cache = Cache;
  gettextCatalog.setCurrentLanguage('vi');

  if (currentUser.isTeacher()) {
    gettextCatalog.setCurrentLanguage('en');
  }
}
