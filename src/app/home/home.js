'use strict';

import HomeCtrl from './home.controller';

function HomeConfig($routeProvider) {
  $routeProvider
    .when('/', {
      title: 'Homepage',
      templateUrl: 'app/home/home.html',
      controller: HomeCtrl,
      controllerAs: 'vm',
      resolve: HomeConfig.resolve
    });
}

HomeConfig.$inject = ['$routeProvider'];

HomeConfig.resolve = {
};

angular
  .module('home', ['ngRoute'])
  .config(HomeConfig);
