'use strict';

import FormManager from '../../libraries/utils/formManager.js';

class HomeCtrl {
  constructor($scope, $routeParams, $popup, $flash, gettextCatalog, Feedback, currentUser) {
    this.$scope = $scope;
    this.$popup = $popup;
    this.$flash = $flash;
    this.gettextCatalog = gettextCatalog;
    this.Feedback = Feedback;
    this.currentUser = currentUser;
    this.formManager = new FormManager(this, 'FeedbackForm');
    if ($routeParams.trigger == 'popup' && $routeParams.name == 'reset_password') {
      this.$popup.create({
        templateUrl: 'libraries/components/popups/resetPassword/resetPassword.html',
        scope: this.$scope,
        scopeValue: {
          token: $routeParams.token
        }
      }).open();
    }
  }
  openSignupPopup() {
    this.$popup.create({
      templateUrl: 'libraries/components/popups/signup/signup.html',
      scope: this.$scope
    }).open();
  }
  formSubmit() {
    if (this.FeedbackForm.$valid) {
      this.Feedback.create(this.feedback).then((res) => {
        if (!res.errors) {
          this.$flash.now(this.gettextCatalog.getString('Chúng tôi sẽ sớm liên lạc bạn'), 'success');
        } else {
          this.formManager.showErrors(res.errors);
        }
      })
    } else {
      this.formManager.errorHandler();
    }
  }
}

HomeCtrl.$inject = ['$scope', '$routeParams', '$popup', '$flash', 'gettextCatalog', 'Feedback', 'currentUser'];

export default HomeCtrl;
