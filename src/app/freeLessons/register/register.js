'use strict';

import FreeLessonsRegisterCtrl from './register.controller';

function FreeLessonsRegisterConfig($routeProvider) {
  $routeProvider
    .when('/free_lessons/register', {
      title: 'Register Free Lessons',
      templateUrl: 'app/freeLessons/register/register.html',
      controller: FreeLessonsRegisterCtrl,
      controllerAs: 'vm',
      resolve: FreeLessonsRegisterConfig.resolve
    });
}

FreeLessonsRegisterConfig.$inject = ['$routeProvider'];

FreeLessonsRegisterConfig.resolve = {
};

angular
  .module('freeLessons.register', ['ngRoute'])
  .config(FreeLessonsRegisterConfig);
