'use strict';

import FormManager from '../../../libraries/utils/formManager';

class FreeLessonsRegisterCtrl {
  constructor($scope, $flash, FreeLessonRegister) {
    this.$flash = $flash;
    this.formManager = new FormManager(this, 'FreeLessonForm');
    this.FreeLessonRegister = FreeLessonRegister;
    this.programs = [
      { name: 'Tiếng Anh giao tiếp', value: 0 },
      { name: 'Tiếng Anh văn phòng', value: 1 },
      { name: 'Tiếng Anh trẻ em', value: 2 },
      { name: 'Luyện thi các chứng chỉ quốc tế (IELTS, TOEFL, TOIEC)', value: 3 },
      { name: 'Tiếng Anh theo nhu cầu', value: 4 }
    ]
    this.user = {};
  }
  formSubmit() {
    var programs = _.map(_.filter(this.programs, (program) => program.selected), (program) => program.value);

    if (programs.length == 0) {
      this.$flash.now('Bạn phải chọn ít nhất một khoá học.', 'error');
      return;
    }
    if (this.FreeLessonForm.$valid) {
      this.FreeLessonRegister.create(_.merge({}, this.user, { programs: programs })).then((res) => {
        if (!res.errors) {
          this.$flash.now('Bạn đã đăng kí thành công', 'success');
          this.user = {};
          _.each(this.programs, (program) => { program.selected = false });
          this.FreeLessonForm.$setPristine();
        } else {
          this.$flash.now(res.errors, 'error');
        }
      })
    } else {
      this.formManager.errorHandler();
    }
  }
}

FreeLessonsRegisterCtrl.$inject = ['$scope', '$flash', 'FreeLessonRegister'];

export default FreeLessonsRegisterCtrl;
