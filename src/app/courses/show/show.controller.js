'use strict';

class CoursesShowCtrl {
  constructor($scope, $routeParams, currentUser, courses, course) {
    this.courses = courses;
    this.course = course;
    this.currentUser = currentUser;
    this.numberOfCourses = courses.length;
    this.numberOfTeachers = _.uniq(_.map(courses, (course) => course.teacherId)).length;
    this.numberOfHours = _.sum(courses, (course) => course.numberOfHours);
    this.lessonSchedules = _.map(this.course.lessonSchedules, (ls) => {
      ls.startedTime = moment(ls.startedTime).format("MMMM DD, YYYY h:mm A");
      ls.endedTime = moment(ls.endedTime).format("MMMM DD, YYYY h:mm A");

      return ls;
    })
  }
  getTagStatus(status) {
    return { official: 'tag-default', insert: 'tag-primary', cancel: 'tag-danger'}[status]
  }
}

CoursesShowCtrl.$inject = ['$scope', '$routeParams', 'currentUser', 'courses', 'course'];

export default CoursesShowCtrl;
