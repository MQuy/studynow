'use strict';

import CoursesShowCtrl from './show.controller';

function CoursesShowConfig($routeProvider, $securityProvider) {
  $routeProvider
    .when('/courses/:id', {
      title: 'Course',
      templateUrl: 'app/courses/show/show.html',
      security: $securityProvider.requireAuthenticated,
      controller: CoursesShowCtrl,
      controllerAs: 'vm',
      resolve: CoursesShowConfig.resolve
    });
}

CoursesShowConfig.$inject = ['$routeProvider', '$securityProvider'];

CoursesShowConfig.resolve = {
  courses: ['currentUser', function(currentUser) {
    return currentUser.getCourses({ 'statuses[]': ['completed', 'cancel'] });
  }],
  course: ['$route', 'Course', function($route, Course) {
    let courseId = $route.current.params.id;

    return Course.get(courseId);
  }]
};

angular
  .module('courses.show', ['ngRoute'])
  .config(CoursesShowConfig);
