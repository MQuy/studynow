'use strict';

require('./index/index');
require('./show/show');
require('./curriculum/curriculum');

angular.module('courses', [
  'courses.index',
  'courses.show',
  'courses.curriculum'
])
