'use strict';

class CoursesCurriculumCtrl {
  constructor($scope, $routeParams, currentUser, courses, course, comments, uploaderObject) {
    this.currentUser = currentUser;
    this.courses = courses;
    this.course = course;
    this.courseNotes = this.course.courseNotes;
    this.comments = comments;
    this.numberOfCourses = courses.length;
    this.numberOfTeachers = _.uniq(_.map(courses, (course) => course.teacherId)).length;
    this.numberOfHours = _.sum(courses, (course) => course.numberOfHours);
    this.uploaderObject = uploaderObject;
  }
}

CoursesCurriculumCtrl.$inject = ['$scope', '$routeParams', 'currentUser', 'courses', 'course', 'comments', 'uploaderObject'];

export default CoursesCurriculumCtrl;
