'use strict';

import CoursesCurriculumCtrl from './curriculum.controller';

function CoursesCurriculum($routeProvider, $securityProvider) {
  $routeProvider
    .when('/courses/:id/curriculum', {
      title: 'Course',
      templateUrl: 'app/courses/curriculum/curriculum.html',
      security: $securityProvider.requireAuthenticated,
      controller: CoursesCurriculumCtrl,
      controllerAs: 'vm',
      resolve: CoursesCurriculum.resolve
    });
}

CoursesCurriculum.$inject = ['$routeProvider', '$securityProvider'];

CoursesCurriculum.resolve = {
  comments: ['$route', 'Comment', function($route, Comment) {
    var paramsUrl = $route.current.params;

    return Comment.all({ CommentableType: 'Course', CommentableId: paramsUrl.id });
  }],
  courses: ['currentUser', function(currentUser) {
    return currentUser.getCourses({ 'statuses[]': ['completed', 'cancel'] });
  }],
  course: ['$route', 'Course', function($route, Course) {
    let courseId = $route.current.params.id;

    return Course.get(courseId);
  }],
  uploaderObject: ['Uploader', function(Uploader) {
    return Uploader.attachFile();
  }]
};

angular
  .module('courses.curriculum', ['ngRoute'])
  .config(CoursesCurriculum);
