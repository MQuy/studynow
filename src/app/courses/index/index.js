'use strict';

import CoursesIndexCtrl from './index.controller';

function CoursesIndexConfig($routeProvider, $securityProvider) {
  $routeProvider
    .when('/courses', {
      title: 'Courses',
      templateUrl: 'app/courses/index/index.html',
      security: $securityProvider.requireAuthenticated,
      controller: CoursesIndexCtrl,
      controllerAs: 'vm',
      resolve: CoursesIndexConfig.resolve
    });
}

CoursesIndexConfig.$inject = ['$routeProvider', '$securityProvider'];

CoursesIndexConfig.resolve = {
  courses: ['currentUser', function(currentUser) {
    return currentUser.getCourses({ 'statuses[]': ['completed', 'cancel'] });
  }]
};

angular
  .module('courses.index', ['ngRoute'])
  .config(CoursesIndexConfig);
