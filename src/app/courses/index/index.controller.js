'use strict';

class CoursesIndexCtrl {
  constructor($scope, courses, currentUser) {
    this.currentUser = currentUser;
    this.courses = courses;
    this.numberOfCourses = courses.length;
    this.numberOfTeachers = _.uniq(_.map(courses, (course) => course.teacherId)).length;
    this.numberOfHours = _.sum(courses, (course) => course.numberOfHours);
  }
}

CoursesIndexCtrl.$inject = ['$scope', 'courses', 'currentUser'];

export default CoursesIndexCtrl;
