require('./app');

import Preload from '../libraries/utils/preload';
import Config from '../libraries/utils/config';

$(function() {
  var tagBody = $('body');
  var tagDom = $('<div id="prebootstrap"/>').appendTo(tagBody);
  var $injector, $q, $location, currentUser;
  var isReady;

  angular.module('config', []).config(['$locationProvider', function($locationProvider) {
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('#');
  }]);

  angular.bootstrap(tagDom, [
    'config',
    'decorators',
    'gettext',
    'services.httpInterceptor',
    'services.localStorage',
    'resources.user',
    'resources.currentUser',
    'resources.config',
  ]);

  $injector = tagDom.injector();
  $q = $injector.get('$q');
  $location = $injector.get('$location');
  currentUser = $injector.get('currentUser');

  $q.all([preloadData()]).then(bootstrapApp);
  setTimeout(() => bootstrapApp(), 20000);

  function preloadData() {
    return currentUser.getSession();
  }

  function preloadConfig() {
    var Config = $injector.get('Config');

    return Config.all();
  }

  function preloadImages() {
    var deferred = $q.defer();

    new Preload(Config.preAssetPaths, {
      cbLoaded: function() {
        deferred.resolve();
      }
    });

    return deferred.promise;
  }

  function bootstrapApp(args = []) {
    angular.element(document).ready(function() {
      if (!isReady) {
        Config.initValue(args[1]);
        isReady = true;
        tagDom.remove();
        angular.bootstrap(document, ['studynow'], { strictDi: true });
        setTimeout(() => { tagBody.removeClass('initialize') }, 1000);
      }
    });
  }
});
