'use strict';

import CheckoutCtrl from './checkout.controller';

function CheckoutConfig($routeProvider) {
  $routeProvider
    .when('/checkout', {
      title: 'Checkout',
      templateUrl: 'app/checkout/checkout.html',
      controller: CheckoutCtrl,
      controllerAs: 'vm',
      resolve: CheckoutConfig.resolve
    });
}

CheckoutConfig.$inject = ['$routeProvider'];

CheckoutConfig.resolve = {
};

angular
  .module('checkout', ['ngRoute'])
  .config(CheckoutConfig);
