'use strict';

class CheckoutCtrl {
  constructor($scope, $popup, currentUser) {
    this.$scope = $scope;
    this.currentUser = currentUser;
  }
}

CheckoutCtrl.$inject = ['$scope', '$popup', 'currentUser'];

export default CheckoutCtrl;
