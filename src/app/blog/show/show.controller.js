'use strict';

class BlogShowCtrl {
  constructor($scope, post) {
    post.publishedAt = moment(post.createdAt).format('MMMM DD, YYYY');
    this.post = post;
    this.postUrl = location.href;

    $scope.$emit('setPageHeader', {
      title: post.title,
      description: post.content
    })
  }
}
BlogShowCtrl.$inject = ['$scope', 'post'];

export default BlogShowCtrl;
