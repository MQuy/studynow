'use strict';

import BlogShowCtrl from './show.controller';

function BlogShowConfig($routeProvider) {
  $routeProvider
    .when('/blog/:id', {
      title: 'Blog',
      templateUrl: 'app/blog/show/show.html',
      controller: BlogShowCtrl,
      controllerAs: 'vm',
      resolve: BlogShowConfig.resolve
    });
}

BlogShowConfig.$inject = ['$routeProvider'];

BlogShowConfig.resolve = {
  post: ['$route', 'Post', function($route, Post) {
    let postId = $route.current.params.id;

    return Post.get(postId);
  }]
};

angular
  .module('blog.show', ['ngRoute'])
  .config(BlogShowConfig);
