'use strict';

class BlogIndexCtrl {
  constructor($routeParams, $scope, posts) {
    this.$routeParams = $routeParams;
    this.posts = _.map(posts, (post) => {
      post.publishedAt = moment(post.createdAt).format('MMMM DD, YYYY');
      return post;
    });
    this.groupPosts = _.chunk(this.posts, 2);
  }
  olderArticles() {
    let urlParams = _.merge({ page: 0 }, this.$routeParams);

    urlParams.page = +urlParams.page + 1;
    return `/blog?${_.toUrlParams(urlParams)}`;
  }
}
BlogIndexCtrl.$inject = ['$routeParams', '$scope', 'posts'];

export default BlogIndexCtrl;
