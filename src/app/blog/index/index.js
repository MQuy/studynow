'use strict';

import BlogIndexCtrl from './index.controller';

function BlogIndexConfig($routeProvider) {
  $routeProvider
    .when('/blog', {
      title: 'Blog',
      templateUrl: 'app/blog/index/index.html',
      controller: BlogIndexCtrl,
      controllerAs: 'vm',
      resolve: BlogIndexConfig.resolve
    });
}

BlogIndexConfig.$inject = ['$routeProvider'];

BlogIndexConfig.resolve = {
  posts: ['$route', 'Post', function($route, Post) {
    let urlParams = $route.current.params;
    let page = +urlParams.page;
    let author = urlParams.author;
    let category = urlParams.category;
    let tag = urlParams.tag;

    return Post.all({ page: page, author: author, category: category, tag: tag });
  }]
};

angular
  .module('blog.index', ['ngRoute'])
  .config(BlogIndexConfig);
