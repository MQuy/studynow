'use strict';

require('./index/index');
require('./show/show');

angular.module('blog', [
  'blog.index',
  'blog.show'
])
