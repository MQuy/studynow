'use strict';

class HomeworkShowCtrl {
  constructor($scope, $routeParams, currentUser, homeworkStudents, comments, homeworkStudent, uploaderObject) {
    this.homeworkStudents = homeworkStudents;
    this.score = _.reduce(this.homeworkStudents, (acc, elem) => acc + +elem.score, 0);
    this.homeworkStudent = homeworkStudent;
    this.comments = comments;
    this.currentUser = currentUser;
    this.uploaderObject = uploaderObject;
  }
}

HomeworkShowCtrl.$inject = ['$scope', '$routeParams', 'currentUser', 'homeworkStudents', 'comments', 'homeworkStudent', 'uploaderObject'];

export default HomeworkShowCtrl;
