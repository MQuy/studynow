'use strict';

import HomeworkShowCtrl from './show.controller';

function HomeworkShowConfig($routeProvider, $securityProvider) {
  $routeProvider
    .when('/homeworks/:id', {
      title: 'Homework',
      templateUrl: 'app/homeworks/show/show.html',
      security: $securityProvider.requireAuthenticated,
      controller: HomeworkShowCtrl,
      controllerAs: 'vm',
      resolve: HomeworkShowConfig.resolve
    });
}

HomeworkShowConfig.$inject = ['$routeProvider', '$securityProvider'];

HomeworkShowConfig.resolve = {
  comments: ['$route', 'Comment', function($route, Comment) {
    var paramsUrl = $route.current.params;

    return Comment.all({ CommentableType: 'HomeworkStudent', CommentableId: paramsUrl.id });
  }],
  homeworkStudents: ['HomeworkStudent', function(HomeworkStudent) {
    return HomeworkStudent.all();
  }],
  homeworkStudent: ['$route', 'HomeworkStudent', function($route, HomeworkStudent) {
    let id = $route.current.params.id;

    return HomeworkStudent.get(id);
  }],
  uploaderObject: ['Uploader', function(Uploader) {
    return Uploader.attachFile();
  }]
};

angular
  .module('homeworks.show', ['ngRoute'])
  .config(HomeworkShowConfig);
