'use strict';

class HomeworksCtrl {
  constructor($scope, homeworkStudents, currentUser) {
    this.currentUser = currentUser;
    this.homeworkStudents = _.map(homeworkStudents, (homeworkStudent) => {
      homeworkStudent.createdAt = moment(homeworkStudent.createdAt).format('DD/MM/YYYY HH:mm')
      return homeworkStudent;
    });
    this.score = _.reduce(this.homeworkStudents, (acc, elem) => acc + +elem.score, 0);
  }
}

HomeworksCtrl.$inject = ['$scope', 'homeworkStudents', 'currentUser'];

export default HomeworksCtrl;
