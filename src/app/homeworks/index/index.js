'use strict';

import HomeworksIndexCtrl from './index.controller';

function HomeworkIndexConfig($routeProvider, $securityProvider) {
  $routeProvider
    .when('/homeworks', {
      title: 'Homework',
      templateUrl: 'app/homeworks/index/index.html',
      security: $securityProvider.requireAuthenticated,
      controller: HomeworksIndexCtrl,
      controllerAs: 'vm',
      resolve: HomeworkIndexConfig.resolve
    });
}

HomeworkIndexConfig.$inject = ['$routeProvider', '$securityProvider'];

HomeworkIndexConfig.resolve = {
  homeworkStudents: ['HomeworkStudent', function(HomeworkStudent) {
    return HomeworkStudent.all();
  }]
};

angular
  .module('homeworks.index', ['ngRoute'])
  .config(HomeworkIndexConfig);
