'use strict';

require('./index/index');
require('./show/show');

angular.module('homeworks', [
  'homeworks.index',
  'homeworks.show'
])
