msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Project-Id-Version: \n"

#: src/app/profile/profile.jade:24
msgid "Account"
msgstr ""

#: src/app/courses/index/index.jade:50
#: src/app/homeworks/index/index.jade:44
msgid "Actions"
msgstr ""

#: src/app/profile/profile.jade:47
msgid "Address"
msgstr ""

#: src/app/homeworks/index/index.jade:39
#: src/app/homeworks/show/show.jade:43
msgid "Attachment"
msgstr ""

#: src/libraries/components/dialogs/book/book.jade:24
#: src/libraries/components/dialogs/books/books.jade:30
msgid "Book"
msgstr ""

#: src/libraries/components/dialogs/books/books.jade:3
msgid "Book Lesson"
msgstr ""

#: src/libraries/components/commons/upcomingSchedules/upcomingSchedules.jade:4
#: src/libraries/components/dialogs/schedule/schedule.jade:22
msgid "Cancel"
msgstr ""

#: src/app/home/home.controller.js:34
msgid "Chúng tôi sẽ sớm liên lạc bạn"
msgstr ""

#: src/app/courses/index/index.jade:40
#: src/app/homeworks/index/index.jade:35
msgid "Code"
msgstr ""

#: src/app/homeworks/index/index.jade:37
msgid "Content"
msgstr ""

#: src/app/profile/profile.jade:63
msgid "Course"
msgstr ""

#: src/libraries/components/dialogs/books/books.jade:14
msgid "Course:"
msgstr ""

#: src/libraries/components/commons/userPanel/userPanel.jade:10
#: src/libraries/components/headers/home/home.jade:18
msgid "Courses"
msgstr ""

#: src/libraries/components/dialogs/book/book.jade:21
#: src/libraries/components/dialogs/books/books.jade:29
#: src/libraries/components/dialogs/schedule/schedule.jade:20
msgid "Date:"
msgstr ""

#: src/app/homeworks/show/show.jade:38
#: src/app/schedule/edit/edit.jade:41
msgid "Description"
msgstr ""

#: src/app/homeworks/index/index.jade:50
#: src/app/homeworks/show/show.jade:44
msgid "Download"
msgstr ""

#: src/app/courses/show/show.jade:50
msgid "Duration"
msgstr ""

#: src/app/profile/profile.jade:29
msgid "Full Name"
msgstr ""

#: src/libraries/components/headers/search/search.jade:9
msgid "Homework"
msgstr ""

#: src/app/homeworks/index/index.jade:29
#: src/app/homeworks/show/show.jade:30
msgid "Homeworks"
msgstr ""

#: src/app/homeworks/index/index.jade:15
#: src/app/homeworks/show/show.jade:15
msgid "Homeworks:"
msgstr ""

#: src/app/courses/curriculum/curriculum.jade:35
#: src/app/courses/index/index.jade:35
#: src/app/courses/show/show.jade:35
msgid "Learned Courses"
msgstr ""

#: src/app/courses/curriculum/curriculum.jade:15
#: src/app/courses/index/index.jade:15
#: src/app/courses/show/show.jade:15
msgid "Learned courses:"
msgstr ""

#: src/libraries/components/headers/search/search.jade:27
msgid "Logout"
msgstr ""

#: src/libraries/components/headers/search/search.jade:42
msgid "Mark as read"
msgstr ""

#: src/app/profile/profile.jade:56
msgid "Money Pocket"
msgstr ""

#: src/app/profile/profile.jade:33
msgid "Nickname"
msgstr ""

#: src/app/homeworks/show/show.jade:53
msgid "Not scored"
msgstr ""

#: src/app/courses/index/index.jade:46
msgid "Number of hours"
msgstr ""

#: src/app/courses/index/index.jade:44
msgid "Number of lessons"
msgstr ""

#: src/app/courses/curriculum/curriculum.jade:25
#: src/app/courses/index/index.jade:25
#: src/app/courses/show/show.jade:25
msgid "Number of partners:"
msgstr ""

#: src/app/courses/index/index.jade:42
msgid "Partner"
msgstr ""

#: src/app/courses/show/show.jade:47
msgid "Partner Name"
msgstr ""

#: src/app/profile/profile.jade:38
msgid "Phone Number"
msgstr ""

#: src/libraries/components/commons/comments/comments.jade:8
msgid "Post"
msgstr ""

#: src/app/profile/profile.jade:21
msgid "Profile"
msgstr ""

#: src/app/schedule/curriculum/curriculum.jade:15
#: src/app/schedule/edit/edit.jade:15
#: src/app/schedule/index/index.jade:13
#: src/app/schedule/show/show.jade:15
#: src/libraries/components/headers/search/search.jade:12
msgid "Schedule"
msgstr ""

#: src/app/homeworks/index/index.jade:41
#: src/app/homeworks/show/show.jade:49
msgid "Score"
msgstr ""

#: src/app/homeworks/index/index.jade:20
#: src/app/homeworks/show/show.jade:20
msgid "Score:"
msgstr ""

#: src/libraries/components/dialogs/books/books.jade:19
msgid "Select Course"
msgstr ""

#: src/app/profile/profile.jade:104
#: src/app/profile/profile.jade:113
#: src/app/profile/profile.jade:81
#: src/app/profile/profile.jade:90
msgid "Select day"
msgstr ""

#: src/app/courses/curriculum/curriculum.jade:20
#: src/app/courses/index/index.jade:20
#: src/app/courses/show/show.jade:20
msgid "Spending Time (h):"
msgstr ""

#: src/app/courses/index/index.jade:48
#: src/app/homeworks/index/index.jade:42
msgid "Started Date"
msgstr ""

#: src/app/courses/show/show.jade:49
msgid "Started Time"
msgstr ""

#: src/app/courses/show/show.jade:52
msgid "Status"
msgstr ""

#: src/app/homeworks/index/index.jade:56
msgid "Submit Homework"
msgstr ""

#: src/app/courses/curriculum/curriculum.jade:42
#: src/app/courses/show/show.jade:41
#: src/app/schedule/curriculum/curriculum.jade:20
#: src/app/schedule/edit/edit.jade:20
#: src/app/schedule/show/show.jade:20
msgid "Textbook"
msgstr ""

#: src/app/homeworks/index/index.jade:59
msgid "There is no homeworks."
msgstr ""

#: src/app/courses/index/index.jade:65
msgid "There is no learned courses."
msgstr ""

#: src/libraries/components/dialogs/book/book.jade:15
#: src/libraries/components/dialogs/books/books.jade:24
#: src/libraries/components/dialogs/schedule/schedule.jade:13
msgid "Time:"
msgstr ""

#: src/app/schedule/edit/edit.jade:36
msgid "Title"
msgstr ""

#: src/app/profile/profile.jade:121
#: src/app/profile/profile.jade:51
#: src/app/schedule/edit/edit.jade:47
msgid "Update"
msgstr ""

#: src/app/profile/profile.controller.js:37
msgid "Update avatar successfully"
msgstr ""

#: src/app/profile/profile.controller.js:82
msgid "Update weekly schedule successfully"
msgstr ""

#: src/app/profile/profile.controller.js:46
msgid "Update your profile successfully"
msgstr ""

#: src/libraries/components/headers/search/search.jade:24
msgid "User Profile"
msgstr ""

#: src/app/courses/index/index.jade:62
msgid "View"
msgstr ""

#: src/libraries/components/commons/userPanel/userPanel.jade:23
msgid "View old courses"
msgstr ""

#: src/app/profile/profile.jade:71
msgid "Weekly Schedule"
msgstr ""

#: src/app/schedule/index/index.controller.js:39
#: src/app/schedule/show/show.controller.js:44
msgid "You can modify your past schedule"
msgstr ""

#: src/app/schedule/index/index.jade:60
#: src/app/schedule/show/show.jade:70
msgid "You do not have any courses at this time"
msgstr ""

#: src/libraries/components/commons/upcomingSchedules/upcomingSchedules.js:28
msgid "You have a schedule at"
msgstr ""

#: src/app/profile/profile.jade:64
msgid "You ony have 1 lesson"
msgid_plural "You only have {{vm.remainOfLessons}} lessons"
msgstr[0] ""
msgstr[1] ""

#: src/app/profile/profile.jade:68
msgid "contact Studynow to have more"
msgstr ""

#: src/app/courses/index/index.jade:60
msgid "hours"
msgstr ""

#: src/app/courses/show/show.jade:61
msgid "minutes"
msgstr ""

#: src/libraries/components/headers/home/home.jade:16
msgid "Introduction"
msgstr ""

msgid "Courses"
msgstr ""

#: src/libraries/components/headers/home/home.jade:20
msgid "Teachers"
msgstr ""

#: src/libraries/components/headers/home/home.jade:22
msgid "Payment"
msgstr ""

#: src/libraries/components/headers/home/home.jade:24
msgid "Contact Us"
msgstr ""

#: src/libraries/components/headers/home/home.jade:30
msgid "Login"
msgstr ""

#: src/libraries/components/commons/upcomingSchedules/upcomingSchedules.js:28
msgid "with"
msgstr ""
